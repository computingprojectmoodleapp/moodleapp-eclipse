package com.moodleapp;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class EditProfile extends Activity implements
		OnItemSelectedListener {

	private static int RESULT_LOAD_IMAGE = 1;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;

	// to get user id and name
	String uid, name;

	// to get the menu item to store user's name
	MenuItem itemUser;

	String itemName;

	// String userID;

	// private ProgressDialog pDialog;

	// JSON_Parse jParser = new JSON_Parse();

	// ArrayList<HashMap<String, String>> productsList;

	// private static String url_all_products = "http://10.0.2.2/UserData.php";

	// JSONArray userData = null;

	String userName;
	String firstName;
	String lastName;
	//String email;
	String emailStop;
	String city;
	String country;
	String timezone;
	String description;
	String emailDisplay;
	String picture;
	
	String picturePath;
	int serverResponseCode = 0;

	Spinner spEmailDisplay;
	Spinner spEmailActivate;
	Spinner countryMap;
	Spinner timezoneMap;
	private ActionBar actionBar;

	JSONObject json;

	static HashMap<String, String> codeHash = new HashMap<String, String>();

	private String[] emailDisp = { "Hide my email address from everyone",
			"Allow everyone to see my email address",
			"Allow only other course members to see my email address" };
	private String[] emailAct = { "This email address is enabled",
			"This email address is disabled" };

	private ProgressDialog pDialog;

	JSON_Parse jParser = new JSON_Parse();

	private String url_all_products = session.strUrl + "UpdateProfile.php";
	String uploadassignment = session.strUrl + "UploadFiles.php";
	String uploadParams = "/0/" + uid + "/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_profile_layout);

		Intent i = getIntent();
		try {
			json = new JSONObject(i.getStringExtra("data"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// object reference to session manager class
		session = new SessionManager(getApplicationContext());

		// get user data from session
		HashMap<String, String> user = session.getUserDetails();
		
        actionBar = getActionBar();

		// Hide the action bar title
        actionBar.setDisplayHomeAsUpEnabled(true);

		// store user id
		uid = user.get(SessionManager.KEY_USERID);

		// store username
		name = user.get(SessionManager.KEY_NAME);

		// message to indicate user's login status
		// Toast.makeText(getApplicationContext(), "User Login Status: " +
		// session.isLoggedIn(),
		// Toast.LENGTH_LONG).show();

		try {
			// Checking for SUCCESS TAG
			userName = json.getString("username");
			firstName = json.getString("firstname");
			lastName = json.getString("lastname");
			city = json.getString("city");
			emailStop = json.getString("emailstop");
			country = json.getString("country");
			timezone = json.getString("timezone");
			description = json.getString("description");
			emailDisplay = json.getString("maildisplay");
			picture = json.getString("picture");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		final EditText txtFirstName = (EditText) findViewById(R.id.txtFirstName);
		txtFirstName.setText(firstName);

		final EditText txtLastName = (EditText) findViewById(R.id.txtLastName);
		txtLastName.setText(lastName);

		final EditText txtCity = (EditText) findViewById(R.id.txtCityTown);
		txtCity.setText(city);

		final EditText txtDescription = (EditText) findViewById(R.id.txtDiscription);
		txtDescription.setText(description);
		
		//final TextView lblProPic = (TextView) findViewById(R.id.lblProfilePicture);
		
		//final CheckBox chRemoveProPic = (CheckBox) findViewById(R.id.chRemoveProPic);
		//if (picture.equals("0"))
		//{
		//	chRemoveProPic.setVisibility(View.INVISIBLE);
		//}
		
		//Button buttonLoadImage = (Button) findViewById(R.id.btnUploadProPic);
		//buttonLoadImage.setOnClickListener(new View.OnClickListener() {

		//	@Override
		//	public void onClick(View arg0) {

				//Intent i = new Intent(
						//Intent.ACTION_PICK,
						//android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				//startActivityForResult(i, RESULT_LOAD_IMAGE);
			//}
		//});
		
		Button btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				firstName = txtFirstName.getText().toString();
				lastName = txtLastName.getText().toString();
				city = txtCity.getText().toString();
				description = txtDescription.getText().toString();
				//if (chRemoveProPic.isChecked())
				//{
//					picture = "0";
				//}
				//if (!lblProPic.getText().equals("Upload Profile Picture"))
				//{
//					new UploadProfilePicture().execute();
				//}
				new UpdateProfile().execute();
			}
		});

		countryMap = (Spinner) findViewById(R.id.spinnerCountry);
		final MyData countryItems[] = new MyData[246];
		countryItems[0] = new MyData("AX", "Aland Island");
		countryItems[1] = new MyData("AF", "Afganistan");
		countryItems[2] = new MyData("AL", "Albania");
		countryItems[3] = new MyData("DZ", "Algeria");
		countryItems[4] = new MyData("AS", "American Samoa");
		countryItems[5] = new MyData("AD", "Andorra");
		countryItems[6] = new MyData("AO", "Angola");
		countryItems[7] = new MyData("AI", "Anguilla");
		countryItems[8] = new MyData("AQ", "Antarctica");
		countryItems[9] = new MyData("AG", "Antigua And Barbuda");
		countryItems[10] = new MyData("AR", "Argentina");
		countryItems[11] = new MyData("AM", "Armenia");
		countryItems[12] = new MyData("AW", "Aruba");
		countryItems[13] = new MyData("AU", "Australia");
		countryItems[14] = new MyData("AT", "Austria");
		countryItems[15] = new MyData("AZ", "Azerbaijon");
		countryItems[16] = new MyData("BS", "Bahamas");
		countryItems[17] = new MyData("BH", "Bahrain");
		countryItems[18] = new MyData("BD", "Bangladesh");
		countryItems[19] = new MyData("BB", "Barbados");
		countryItems[20] = new MyData("BY", "Belarus");
		countryItems[21] = new MyData("BE", "Belgium");
		countryItems[22] = new MyData("BZ", "Belize");
		countryItems[23] = new MyData("BJ", "Benin");
		countryItems[24] = new MyData("BM", "Bermuda");
		countryItems[25] = new MyData("BT", "Bhutan");
		countryItems[26] = new MyData("BO", "Bolivia");
		countryItems[27] = new MyData("BA", "Bosnia And Herzegovina");
		countryItems[28] = new MyData("BW", "Botswana");
		countryItems[29] = new MyData("BV", "Bouvet Island");
		countryItems[30] = new MyData("BR", "Brazil");
		countryItems[31] = new MyData("IO", "British Indian Ocean Territory");
		countryItems[32] = new MyData("BN", "Brunei Darussalam");
		countryItems[33] = new MyData("BG", "Bulgaria");
		countryItems[34] = new MyData("BF", "Burkina Faso");
		countryItems[35] = new MyData("BI", "Burundi");
		countryItems[36] = new MyData("CI", "Cote D' Ivoire");
		countryItems[37] = new MyData("KH", "Cambodia");
		countryItems[38] = new MyData("CM", "Cameroon");
		countryItems[39] = new MyData("CA", "Canada");
		countryItems[40] = new MyData("CV", "Cape Verde");
		countryItems[41] = new MyData("KY", "Cayman Islands");
		countryItems[42] = new MyData("CF", "Central African Republic");
		countryItems[43] = new MyData("TD", "Chad");
		countryItems[44] = new MyData("CL", "Chile");
		countryItems[45] = new MyData("CN", "China");
		countryItems[46] = new MyData("CX", "Christmas Island");
		countryItems[47] = new MyData("CC", "Cocos(Keeling)Islands");
		countryItems[48] = new MyData("CO", "Colombia");
		countryItems[49] = new MyData("KM", "Comoros");
		countryItems[50] = new MyData("CG", "Congo");
		countryItems[51] = new MyData("CD",
				"Congo,The Democratic Republic Of The");
		countryItems[52] = new MyData("CK", "Cook Islands");
		countryItems[53] = new MyData("CR", "Costa Rica");
		countryItems[54] = new MyData("HR", "Croatia");
		countryItems[55] = new MyData("CU", "Cuba");
		countryItems[56] = new MyData("CY", "Cyprus");
		countryItems[57] = new MyData("CZ", "Czech Republic");
		countryItems[58] = new MyData("DK", "Denmark");
		countryItems[59] = new MyData("DJ", "Djibouti");
		countryItems[60] = new MyData("DM", "Dominica");
		countryItems[61] = new MyData("DO", "Dominican Republic");
		countryItems[62] = new MyData("EC", "Ecuador");
		countryItems[63] = new MyData("EG", "Egypt");
		countryItems[64] = new MyData("SV", "El Salvador");
		countryItems[65] = new MyData("GQ", "Equatorial Guinea");
		countryItems[66] = new MyData("ER", "Eritrea");
		countryItems[67] = new MyData("EE", "Estonia");
		countryItems[68] = new MyData("ET", "Ethiopia");
		countryItems[69] = new MyData("FK", "Falkland Islands Malvinas");
		countryItems[70] = new MyData("FO", "Faroe Islands");
		countryItems[71] = new MyData("FJ", "Fiji");
		countryItems[72] = new MyData("FI", "Finland");
		countryItems[73] = new MyData("FR", "France");
		countryItems[74] = new MyData("GF", "French Guiana");
		countryItems[75] = new MyData("PF", "French Polynesia");
		countryItems[76] = new MyData("TF", "French Southern Territories");
		countryItems[77] = new MyData("GA", "Gabon");
		countryItems[78] = new MyData("GM", "Gambia");
		countryItems[79] = new MyData("GE", "Georgia");
		countryItems[80] = new MyData("DE", "Germany");
		countryItems[81] = new MyData("GH", "Ghana");
		countryItems[82] = new MyData("GI", "Gibraltar");
		countryItems[83] = new MyData("GR", "Greece");
		countryItems[84] = new MyData("GL", "Greenland");
		countryItems[85] = new MyData("GD", "Grenada");
		countryItems[86] = new MyData("GP", "Guadeloupe");
		countryItems[87] = new MyData("GU", "Guam");
		countryItems[88] = new MyData("GT", "Guatemala");
		countryItems[89] = new MyData("GG", "Guernsey");
		countryItems[90] = new MyData("GN", "Guinea");
		countryItems[91] = new MyData("GW", "Guinea Bissau");
		countryItems[92] = new MyData("GY", "Guyana");
		countryItems[93] = new MyData("HT", "Haiti");
		countryItems[94] = new MyData("HM", "Heard Island Mcdonald Islands");
		countryItems[95] = new MyData("VA", "Holy See(Vatican City State");
		countryItems[96] = new MyData("HN", "Honduras");
		countryItems[97] = new MyData("HK", "HongKong");
		countryItems[98] = new MyData("HU", "Hungary");
		countryItems[99] = new MyData("IS", "Ice Land");
		countryItems[100] = new MyData("IN", "India");
		countryItems[101] = new MyData("ID", "Indonesia");
		countryItems[102] = new MyData("IR", "Iran,Islamic Republic of");
		countryItems[103] = new MyData("IQ", "Iraq");
		countryItems[104] = new MyData("IE", "Ireland");
		countryItems[105] = new MyData("IM", "Isle of Man");
		countryItems[106] = new MyData("IL", "Israel");
		countryItems[107] = new MyData("IT", "Italy");
		countryItems[108] = new MyData("JM", "Jamaica");
		countryItems[109] = new MyData("JP", "Japan");
		countryItems[110] = new MyData("JE", "Jersey");
		countryItems[111] = new MyData("JO", "Jordan");
		countryItems[112] = new MyData("KZ", "Kazakhstan");
		countryItems[113] = new MyData("KE", "Kenya");
		countryItems[114] = new MyData("KI", "Kiribati");
		countryItems[115] = new MyData("KP",
				"Korea,Democratic People's Republic of");
		countryItems[116] = new MyData("KR", "Korea,Republic of");
		countryItems[117] = new MyData("KW", "Kuwait");
		countryItems[118] = new MyData("KG", "Kyrgystan");
		countryItems[119] = new MyData("LA", "Lao People's Democratic Republic");
		countryItems[120] = new MyData("LV", "Latvia");
		countryItems[121] = new MyData("LB", "Lebanon");
		countryItems[122] = new MyData("LS", "Lesotho");
		countryItems[123] = new MyData("LR", "Liberia");
		countryItems[124] = new MyData("LY", "Libyan Arab Jamahiriya");
		countryItems[125] = new MyData("LI", "Liechtenstein");
		countryItems[126] = new MyData("LT", "Lithuania");
		countryItems[127] = new MyData("LU", "Luxembourg");
		countryItems[128] = new MyData("MO", "Macao");
		countryItems[129] = new MyData("MK",
				"Macedonia,The Former Yugoslav Republic of");
		countryItems[130] = new MyData("MG", "Madagascar");
		countryItems[131] = new MyData("MW", "Malawi");
		countryItems[132] = new MyData("MY", "Malaysia");
		countryItems[133] = new MyData("MV", "Maldives");
		countryItems[134] = new MyData("ML", "Mali");
		countryItems[135] = new MyData("MT", "Malta");
		countryItems[136] = new MyData("MH", "Marshall  Islands");
		countryItems[137] = new MyData("MQ", "Martinique");
		countryItems[138] = new MyData("MR", "Mauritania");
		countryItems[139] = new MyData("MU", "Mauritius");
		countryItems[140] = new MyData("YT", "Mayotte");
		countryItems[141] = new MyData("MX", "Mexico");
		countryItems[142] = new MyData("FM", "Micronesia,Federated States of");
		countryItems[143] = new MyData("MD", "Moldova,Republlic of");
		countryItems[144] = new MyData("MC", "Monaco");
		countryItems[145] = new MyData("MN", "Mongolia");
		countryItems[146] = new MyData("ME", "Montenegro");
		countryItems[147] = new MyData("MS", "Montserrat");
		countryItems[148] = new MyData("MA", "Morocco");
		countryItems[149] = new MyData("MZ", "Mozambique");
		countryItems[150] = new MyData("MM", "Myanmar");
		countryItems[151] = new MyData("NA", "Namibia");
		countryItems[152] = new MyData("NR", "Nauru");
		countryItems[153] = new MyData("NP", "Nepal");
		countryItems[154] = new MyData("NL", "Netherlands");
		countryItems[155] = new MyData("AN", "Netherlands Antilles");
		countryItems[156] = new MyData("NC", "New Caledonia");
		countryItems[157] = new MyData("NZ", "New Zealand");
		countryItems[158] = new MyData("NI", "Nicaragua");
		countryItems[159] = new MyData("NE", "Niger");
		countryItems[160] = new MyData("NG", "Nigeria");
		countryItems[161] = new MyData("NU", "Niue");
		countryItems[162] = new MyData("NF", "Norfolk Island");
		countryItems[163] = new MyData("MP", "Northern Mariana Islands");
		countryItems[164] = new MyData("NO", "Norway");
		countryItems[165] = new MyData("OM", "Oman");
		countryItems[166] = new MyData("PK", "Pakistan");
		countryItems[167] = new MyData("PW", "Palau");
		countryItems[168] = new MyData("PS", "Palestinian Territory Occupied");
		countryItems[169] = new MyData("PA", "Panama");
		countryItems[170] = new MyData("PG", "Papua New Guinea");
		countryItems[171] = new MyData("PY", "Paraguay");
		countryItems[172] = new MyData("PE", "Peru");
		countryItems[173] = new MyData("PH", "Philippines");
		countryItems[174] = new MyData("PN", "Pitcairn");
		countryItems[175] = new MyData("PL", "Poland");
		countryItems[176] = new MyData("PT", "Portugal");
		countryItems[177] = new MyData("PR", "Puerto Rico");
		countryItems[178] = new MyData("QA", "Qatar");
		countryItems[179] = new MyData("RE", "Reunion");
		countryItems[180] = new MyData("RO", "Romania");
		countryItems[181] = new MyData("RU", "Russian Federation");
		countryItems[182] = new MyData("RW", "Rwanda");
		countryItems[183] = new MyData("BL", "Saint Barthelemy");
		countryItems[184] = new MyData("SH", "Saint Helena");
		countryItems[185] = new MyData("KN", "Saint Kitts And Nevis");
		countryItems[186] = new MyData("LC", "Saint Lucia");
		countryItems[187] = new MyData("MF", "Saint Martin");
		countryItems[188] = new MyData("PM", "Saint Pierre And Miquelon");
		countryItems[189] = new MyData("VC", "Saint Vincent And The Grenadines");
		countryItems[190] = new MyData("WS", "Samoa");
		countryItems[191] = new MyData("SM", "San Marino");
		countryItems[192] = new MyData("ST", "Sao Tome And Principe");
		countryItems[193] = new MyData("SA", "Saudi Arabia");
		countryItems[194] = new MyData("SN", "Senegal");
		countryItems[195] = new MyData("RS", "Serbia");
		countryItems[196] = new MyData("SC", "Seychelles");
		countryItems[197] = new MyData("SL", "Sierra Leone");
		countryItems[198] = new MyData("SG", "Singapore");
		countryItems[199] = new MyData("SK", "Slovakia");
		countryItems[200] = new MyData("SI", "Slovenia");
		countryItems[201] = new MyData("SB", "Soloman Islands");
		countryItems[202] = new MyData("SO", "Somalia");
		countryItems[203] = new MyData("ZA", "South Africa");
		countryItems[204] = new MyData("GS",
				"South Georgia And The South Sandwich Islands");
		countryItems[205] = new MyData("ES", "Spain");
		countryItems[206] = new MyData("LK", "Srilanka");
		countryItems[207] = new MyData("SD", "Sudan");
		countryItems[208] = new MyData("SR", "Suriname");
		countryItems[209] = new MyData("SJ", "Svalbard And Jan Mayen");
		countryItems[210] = new MyData("SZ", "Swaziland");
		countryItems[211] = new MyData("SE", "Sweden");
		countryItems[212] = new MyData("CH", "Switzerland");
		countryItems[213] = new MyData("SY", "Syrian Arab Republic");
		countryItems[214] = new MyData("TW", "Taiwan");
		countryItems[215] = new MyData("TJ", "Tajikistan");
		countryItems[216] = new MyData("TZ", "Tanzania,United Republic of");
		countryItems[217] = new MyData("TH", "Thailand");
		countryItems[218] = new MyData("TL", "Timor-Leste");
		countryItems[219] = new MyData("TG", "Togo");
		countryItems[220] = new MyData("TK", "Tokelau");
		countryItems[221] = new MyData("TO", "Tonga");
		countryItems[222] = new MyData("TT", "Trinidad And Tobago");
		countryItems[223] = new MyData("TN", "Tunisia");
		countryItems[224] = new MyData("TR", "Turkey");
		countryItems[225] = new MyData("TM", "Turkmenistan");
		countryItems[226] = new MyData("TC", "Turks And Caicos Islands");
		countryItems[227] = new MyData("TV", "Tuvalu");
		countryItems[228] = new MyData("UG", "Uganda");
		countryItems[229] = new MyData("UA", "Ukraine");
		countryItems[230] = new MyData("AE", "United Arab Emirates");
		countryItems[231] = new MyData("GB", "United Kingdom");
		countryItems[232] = new MyData("US", "United States");
		countryItems[233] = new MyData("UM",
				"United States Minor Outlying Islands");
		countryItems[234] = new MyData("UY", "Uruguay");
		countryItems[235] = new MyData("UZ", "Uzbekistan");
		countryItems[236] = new MyData("VU", "Vanuatu");
		countryItems[237] = new MyData("VE", "Venezuela");
		countryItems[238] = new MyData("VN", "Viet Nam");
		countryItems[239] = new MyData("VG", "Virgin Islands,British");
		countryItems[240] = new MyData("VI", "Virgin Islands,U.S");
		countryItems[241] = new MyData("WF", "Wallis And Futuna");
		countryItems[242] = new MyData("EH", "Western Sahara");
		countryItems[243] = new MyData("YE", "Yemen");
		countryItems[244] = new MyData("ZM", "Zambia");
		countryItems[245] = new MyData("ZW", "Zimbabwe");
		// items[10] = new MyData( "Albania","AL" );
		ArrayAdapter<MyData> countryAdapter = new ArrayAdapter<MyData>(this,
				android.R.layout.simple_spinner_item, countryItems);
		countryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countryMap.setAdapter(countryAdapter);
		countryMap
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						MyData countryData = countryItems[position];
						// valueTextView.setText( d.getValue() );

						country = countryData.getKey().toString();
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		
		for (int position = 0; position <= countryItems.length - 1; position++) {
			MyData countryPos = countryItems[position];
			if (countryPos.getKey().equals(country)) {
				countryMap.setSelection(position);
			}
		}

		timezoneMap = (Spinner) findViewById(R.id.spinnerTimeZone);
		final MyData timezoneItems[] = new MyData[53];
		timezoneItems[0] = new MyData("-13.0", "UTC-13");
		timezoneItems[1] = new MyData("-12.5", "UTC-12.5");
		timezoneItems[2] = new MyData("-12.0", "UTC-12");
		timezoneItems[3] = new MyData("-11.5", "UTC-11.5");
		timezoneItems[4] = new MyData("-11.0", "UTC-11");
		timezoneItems[5] = new MyData("-10.5", "UTC-10.5");
		timezoneItems[6] = new MyData("-10.0", "UTC-10");
		timezoneItems[7] = new MyData("-9.5",  "UTC-9.5");
		timezoneItems[8] = new MyData("-9.0", "UTC-9");
		timezoneItems[9] = new MyData("-8.5", "UTC-8.5");
		timezoneItems[10] = new MyData("-8.0", "UTC-8.0");
		timezoneItems[11] = new MyData("-7.5", "UTC-7.5");
		timezoneItems[12] = new MyData("-7.0", "UTC-7");
		timezoneItems[13] = new MyData("-6.5", "UTC-6.5");
		timezoneItems[14] = new MyData("-6.0", "UTC-6");
		timezoneItems[15] = new MyData("-5.5", "UTC-5.5");
		timezoneItems[16] = new MyData("-5.0", "UTC-5");
		timezoneItems[17] = new MyData("-4.5", "UTC-4.5");
		timezoneItems[18] = new MyData("-4.0", "UTC-4");
		timezoneItems[19] = new MyData("-3.5", "UTC-3.5");
		timezoneItems[20] = new MyData("-3.0", "UTC-3");
		timezoneItems[21] = new MyData("-2.5", "UTC-2.5");
		timezoneItems[22] = new MyData("-2.0", "UTC-2");
		timezoneItems[23] = new MyData("-1.5", "UTC-1.5");
		timezoneItems[24] = new MyData("-1.0", "UTC-1");
		timezoneItems[25] = new MyData("-0.5", "UTC-0.5");
		timezoneItems[26] = new MyData("0.0", "UTC");
		timezoneItems[27] = new MyData("0.5", "UTC+0.5");
		timezoneItems[28] = new MyData("1.0", "UTC+1");
		timezoneItems[29] = new MyData("1.5", "UTC+1.5");
		timezoneItems[30] = new MyData("2.0", "UTC+2");
		timezoneItems[31] = new MyData("2.5", "UTC+2.5");
		timezoneItems[32] = new MyData("3.0", "UTC+3");
		timezoneItems[33] = new MyData("3.5", "UTC+3.5");
		timezoneItems[34] = new MyData("4.0", "UTC+4");
		timezoneItems[35] = new MyData("4.5",  "UTC+4.5");
		timezoneItems[36] = new MyData("5.0", "UTC+5");
		timezoneItems[37] = new MyData("5.5", "UTC+5.5");
		timezoneItems[38] = new MyData("6.0", "UTC+6.0");
		timezoneItems[39] = new MyData("6.5", "UTC+6.5");
		timezoneItems[40] = new MyData("7.0", "UTC+7");
		timezoneItems[41] = new MyData("7.5", "UTC+7.5");
		timezoneItems[42] = new MyData("8.0", "UTC+8");
		timezoneItems[43] = new MyData("8.5", "UTC+8.5");
		timezoneItems[44] = new MyData("9.0", "UTC+9");
		timezoneItems[45] = new MyData("9.5", "UTC+9.5");
		timezoneItems[46] = new MyData("10.0", "UTC+10");
		timezoneItems[47] = new MyData("10.5", "UTC+10.5");
		timezoneItems[48] = new MyData("11.0", "UTC+11");
		timezoneItems[49] = new MyData("11.5", "UTC+11.5");
		timezoneItems[50] = new MyData("12.0", "UTC+12");
		timezoneItems[51] = new MyData("12.5", "UTC+12.5");
		timezoneItems[52] = new MyData("13.0", "UTC+13");
		// items[10] = new MyData( "Albania","AL" );
		ArrayAdapter<MyData> timezoneAdapter = new ArrayAdapter<MyData>(this,
				android.R.layout.simple_spinner_item, timezoneItems);
		timezoneAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		timezoneMap.setAdapter(timezoneAdapter);
		timezoneMap
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						MyData timezoneData = timezoneItems[position];
						// valueTextView.setText( d.getValue() );
						timezone = timezoneData.getKey().toString();
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});	

		for (int position = 0; position <= timezoneItems.length - 1; position++) {
			MyData timezonePos = timezoneItems[position];
			if (timezonePos.getKey().equals("Algeria")) {
				timezoneMap.setSelection(position);
			}
		}

		spEmailDisplay = (Spinner) findViewById(R.id.spinnerEmailDisplay);
		ArrayAdapter<String> adapterEmailDisplay = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, emailDisp);
		adapterEmailDisplay
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spEmailDisplay.setAdapter(adapterEmailDisplay);
		spEmailDisplay.setSelection(Integer.parseInt(emailDisplay));
		spEmailDisplay
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						emailDisplay = String.valueOf(spEmailDisplay
								.getSelectedItemPosition());
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

		spEmailActivate = (Spinner) findViewById(R.id.spinnerEmailActivated);
		ArrayAdapter<String> adapterEmailActivate = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, emailAct);
		adapterEmailActivate
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spEmailActivate.setAdapter(adapterEmailActivate);
		spEmailActivate.setSelection(Integer.parseInt(emailStop));
		spEmailActivate
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						emailStop = String.valueOf(spEmailActivate
								.getSelectedItemPosition());
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
	}
	
	class UpdateProfile extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditProfile.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("id", uid));
			params.add(new BasicNameValuePair("firstname", firstName));
			params.add(new BasicNameValuePair("lastname", lastName));
			params.add(new BasicNameValuePair("picture", picture));
			params.add(new BasicNameValuePair("emaildisplay", emailDisplay));
			params.add(new BasicNameValuePair("emailactivated", emailStop));
			params.add(new BasicNameValuePair("citytown", city));
			params.add(new BasicNameValuePair("country", country));
			params.add(new BasicNameValuePair("timezone", timezone));
			params.add(new BasicNameValuePair("description", description));
			// getting JSON string from URL

			json = jParser.makeHttpRequest(url_all_products, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			Log.d("id", uid);
			Log.d("firstname", firstName);
			Log.d("lastname", lastName);
			Log.d("picture", picture);
			Log.d("emaildisplay", emailDisplay);
			Log.d("emailactivated", emailStop);
			Log.d("citytown", city);
			Log.d("country", country);
			Log.d("timezone", timezone);
			Log.d("description", description);

			/*
			 * try { // Checking for SUCCESS TAG int success =
			 * json.getInt("success");
			 * 
			 * if (success == 1) { userName = json.getString("username");
			 * fullName = json.getString("firstname") + " " +
			 * json.getString("lastname"); email = json.getString("email"); city
			 * = json.getString("city"); country = json.getString("country");
			 * picture = json.getString("picture"); } else { Log.e("Error",
			 * "Load User Data"); } } catch (JSONException e) {
			 * e.printStackTrace(); }
			 */

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			finish();
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					// ListAdapter adapter = new SimpleAdapter(Profile.this,
					// productsList, R.layout.list_item, new String[] {TAG_PID,
					// TAG_NAME, "reference"}, new int[] { R.id.id, R.id.name,
					// R.id.reference });
					// updating listview
					// setListAdapter(adapter);
				}
			});

		}
	}
	
	class UploadProfilePicture extends AsyncTask<String, String, String> {

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(picturePath);
		private FileInputStream fileInputStream;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EditProfile.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			// open a URL connection to the Servlet
			try {
				fileInputStream = new FileInputStream(sourceFile);
				URL url = new URL(uploadassignment + "?subjectID=" + uploadParams + "&type=1");

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", picturePath);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=uploaded_file;filename="
						+ picturePath + "" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);
				
				/*if (tv.getText().equals("No Files Uploaded"))
					update = "0";
				else 
					update = "1";
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("update", update));
				params.add(new BasicNameValuePair("assignmentID", itemInstance));
				params.add(new BasicNameValuePair("userID", uid));
				
				JSONObject json = jParser.makeHttpRequest(uploadAssignmentTableUpdate, "GET",
						params);
				
				Log.d("Upload Table Update: ", json.toString());*/
			} catch (Exception e) {

			}
			return null;
		}

		/**
		 * Updating progress bar
		 * */
		/*
		 * protected void onProgressUpdate(String... progress) { // setting
		 * progress percentage
		 * pDialog.setProgress(Integer.parseInt(progress[0])); }
		 */

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();
			
			finish();
			startActivity(getIntent());
			// statusUpload = true;
			// new UploadTableUpdate().execute();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
				&& null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			picturePath = cursor.getString(columnIndex);
			cursor.close();

			// ImageView imageView = (ImageView) findViewById(R.id.imageView1);
			// imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.icon_menu, menu);

		if (session.isLoggedIn() == true) {
			menu.add(0, 1, Menu.NONE, name);
			menu.add(0, 4, Menu.NONE, "Refresh");
			menu.add(0, 2, Menu.NONE, "Logout");
			getMenuInflater().inflate(R.menu.logged_in, menu);
		} else {
			menu.add(0, 3, Menu.NONE, "Login");
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
    	if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(), MainTabs.class);
			startActivity(profile);
			return true;
		}

		if (id == 1) {
			Intent profile = new Intent(getApplicationContext(), Profile.class);
			startActivity(profile);
			return true;
		}

		if (id == 2) {
			session.logoutUser();
			return true;
		}

		if (id == 3) {
			Intent loginAct = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(loginAct);
			return true;
		}

		if (id == 4) {
			finish();
			startActivity(getIntent());
			return true;
		}

		if (id == R.id.action_mail) {
			Intent msg = new Intent(getApplicationContext(), Message.class);
			startActivity(msg);
			return true;
		}

		if (id == R.id.ic_action_event) {
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null,
					//CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(),CalendarActivity.class);
			startActivity(calendar);
		}

		return super.onOptionsItemSelected(item);
	}

	class MyData {
		public MyData(String itemKey, String itemValue) {
			this.itemValue = itemValue;
			this.itemKey = itemKey;
			// this.itemPosition = itemPosition;
		}

		public String getValue() {
			return itemValue;
		}

		public String getKey() {
			return itemKey;
		}

		public String toString() {
			return itemValue;
		}

		String itemValue;
		String itemKey;
		// int itemPosition;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}

}
