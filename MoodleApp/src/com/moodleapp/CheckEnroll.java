package com.moodleapp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class CheckEnroll extends Activity {
	
	// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();
		
		// Session Manager Class
		SessionManager session;
		
		//to get user id and name
		String uid, name;
		
		//to get the menu item to store user's name
		MenuItem itemUser;

	String subjectID ;
	
	String userID ;
	
	String itemName;
	
	int success;
	
	private ActionBar actionBar;
	
//	String display;
	
	private ProgressDialog pDialog;
	
	
	
	JSON_Parse jParser = new JSON_Parse();
	 
    ArrayList<HashMap<String, String>> productsList;
 
    private String url_all_products = session.strUrl + "CheckEnroll.php";
 
    private static final String TAG_SUCCESS = "success";
   // private static final String TAG_PRODUCTS = "display";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "name";
 
    //JSONArray display = null;	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//object reference to session manager class
  		session = new SessionManager(getApplicationContext());
  		
  		//check login
      	//session.checkLogin();
     
          // get user data from session
          HashMap<String, String> user = session.getUserDetails();
          
          //store user id
          uid = user.get(SessionManager.KEY_USERID);
          
          //store username
          name = user.get(SessionManager.KEY_NAME);
          
         /* //message to indicate user's login status
  	    Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), 
  	        		Toast.LENGTH_LONG).show(); */
		
        Intent i = getIntent();
        
        // getting product id (pid) from intent
        subjectID = i.getStringExtra(TAG_PID);
        itemName = i.getStringExtra(TAG_NAME);
        userID = "3";
		
		productsList = new ArrayList<HashMap<String, String>>();
		 
        // Loading products in Background Thread
        new LoadAllProducts().execute();
        
    	
        actionBar = getActionBar();

		// Hide the action bar title
        actionBar.setDisplayHomeAsUpEnabled(true);
        
//        lv.setOnItemClickListener(new OnItemClickListener() {

//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				
//				String item_ID = ((TextView)view.findViewById(R.id.id)).getText().toString();
//				String item_Name = ((TextView) view.findViewById(R.id.name)).getText().toString();
				
//				Intent in = new Intent(getApplicationContext(), Years.class);
				
//				in.putExtra(TAG_PID, item_ID);
//				in.putExtra(TAG_NAME, item_Name);
			
//				startActivityForResult(in, 100);
//			}        	
//		});
	}
	
	class LoadAllProducts extends AsyncTask<String, String, String> {
   	 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CheckEnroll.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
        	List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("subjectID", subjectID));
            params.add(new BasicNameValuePair("userID", userID));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                success = json.getInt(TAG_SUCCESS);
            	//display = json.getString(TAG_PRODUCTS);
                
            	   
//                } else {
                    // no products found
                    // Launch Add New product Activity
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					if (success == 1) {
						Intent in = new Intent(getApplicationContext(), SubjectDetails.class);						
						in.putExtra(TAG_PID, subjectID);
						in.putExtra(TAG_NAME, itemName);					
						startActivityForResult(in, 100);
	            	   }
	            	   else {
	            		   Intent in = new Intent(getApplicationContext(), EnrollSubject.class);							
							in.putExtra(TAG_PID, subjectID);
							in.putExtra(TAG_NAME, itemName);						
							startActivityForResult(in, 100);
	            	   }
				}
			});

		}
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
		getMenuInflater().inflate(R.menu.icon_menu, menu);


    	if(session.isLoggedIn() == true)
    	{
    		menu.add(0, 1, Menu.NONE, name);
    		menu.add(0, 4, Menu.NONE, "Refresh");
    		menu.add(0, 2, Menu.NONE, "Logout");
    		getMenuInflater().inflate(R.menu.logged_in, menu);
    	}
    	else
    	{
    		menu.add(0, 3, Menu.NONE, "Login");
    	}

		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	int id = item.getItemId();
    	
    	if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(), MainTabs.class);
			startActivity(profile);
			return true;
		}
		
    	if (id == 1) {
			Intent profile = new Intent(getApplicationContext(),Profile.class);
			startActivity(profile);
			return true;
		}
		
		if(id == 2){
			session.logoutUser();
			return true;
		}
		
		if(id == 3)
		{
			Intent loginAct = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(loginAct);
			return true;
		}
		
		if(id == 4)
    	{
    		finish();
    		startActivity(getIntent());
    		return true;
    	}
		
		if (id == R.id.action_mail)
		{
			Intent msg = new Intent(getApplicationContext(),Message.class);
			startActivity(msg);
			return true;
		}
		
		if(id == R.id.ic_action_event)
		{
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(), CalendarActivity.class);
			startActivity(calendar);
		}
	
	
	return super.onOptionsItemSelected(item);
    }

}
