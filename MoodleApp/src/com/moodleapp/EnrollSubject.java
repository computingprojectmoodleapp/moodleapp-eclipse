package com.moodleapp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class EnrollSubject extends Activity {
	
	// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();
		
		// Session Manager Class
		SessionManager session;
		
		//to get user id and name
		String uid, name;
		
		//to get the menu item to store user's name
		MenuItem itemUser;

	String subjectID ;
	
	String userID ;
	
	String itemName;
	
	String pass;
	
	private ProgressDialog pDialog;
	
	JSON_Parse jParser = new JSON_Parse();
	
	 
    ArrayList<HashMap<String, String>> productsList;
 
    private String url_all_products = session.strUrl + "EnrollKey.php";
    
    private String url_all_products2 = session.strUrl + "UpdateEnrollment.php";
 
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "enrollkey";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "name";
 
    String enrollKey;	
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enroll_subject_layout);
		
		//object reference to session manager class
  		session = new SessionManager(getApplicationContext());
  		
  		//check login
      	//session.checkLogin();
     
          // get user data from session
          HashMap<String, String> user = session.getUserDetails();
          
          //store user id
          uid = user.get(SessionManager.KEY_USERID);
          
          //store username
          name = user.get(SessionManager.KEY_NAME);
          
        /*  //message to indicate user's login status
  	    Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), 
  	        		Toast.LENGTH_LONG).show(); */
		
		CheckBox cbPass = (CheckBox) findViewById(R.id.cbUnmask);
	    Button btnEnrollMe = (Button) findViewById(R.id.btnEnroll);
	    final TextView tvPass = (TextView) findViewById(R.id.txtPassword);
	    TextView tv = (TextView) findViewById(R.id.lblCourses);
				
        Intent i = getIntent();
        
        // getting product id (pid) from intent
        subjectID = i.getStringExtra(TAG_PID);
        itemName = i.getStringExtra(TAG_NAME);
        userID = "3";
        
       //TextView tv = (TextView) findViewById(R.id.lblCourses);
		tv.setText(itemName);
		
		
		
//		productsList = new ArrayList<HashMap<String, String>>();
		 
        // Loading products in Background Thread
//        new LoadAllProducts().execute();
		
		cbPass.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	        @Override
	        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	            if(isChecked) {
	                tvPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
	            } else {
	                tvPass.setInputType(129);
	            }
	        }
	    });
		
		
		btnEnrollMe.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				pass = tvPass.getText().toString();
				new LoadAllProducts().execute();			
			}
		});
	}
	
	

	class LoadAllProducts extends AsyncTask<String, String, String> {
   	 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EnrollSubject.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
        	List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("subjectID", subjectID));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    // products found
                    // Getting Array of Products
                	enrollKey = json.getString(TAG_PRODUCTS);
 
                    // looping through All Products
               
                } else {
                    // no products found
                    // Launch Add New product Activity
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();		
			
			if (pass.equals(enrollKey))
        	{
				new UpdateEnrollment().execute();
        		Intent in = new Intent(getApplicationContext(), SubjectDetails.class);						
        		in.putExtra(TAG_PID, subjectID);
        		in.putExtra(TAG_NAME, itemName);					
        		startActivityForResult(in, 100);         	  
        	}
        	else {
        		AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(EnrollSubject.this);
                dlgAlert.setMessage("Incorrect Enrollment Key!");
                dlgAlert.setTitle("Error Message...");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                    }
                });
        	}
		}
	}
	
	class UpdateEnrollment extends AsyncTask<String, String, String> {
	   	 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EnrollSubject.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
        	List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("subjectID", subjectID));
            params.add(new BasicNameValuePair("userID", userID));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products2, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    // products found
                    // Getting Array of Products
                	//enrollKey = json.getString(TAG_PRODUCTS);
 
                    // looping through All Products
               
                } else {
                	AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(EnrollSubject.this);
                    dlgAlert.setMessage("Cannot Continiue...");
                    dlgAlert.setTitle("Error Message...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();		
		}
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
		getMenuInflater().inflate(R.menu.icon_menu, menu);
    	
    	if(session.isLoggedIn() == true)
    	{
    		menu.add(0, 1, Menu.NONE, name);
    		menu.add(0, 4, Menu.NONE, "Refresh");
    		menu.add(0, 2, Menu.NONE, "Logout");
    		getMenuInflater().inflate(R.menu.logged_in, menu);
    	}
    	else
    	{
    		menu.add(0, 3, Menu.NONE, "Login");
    	}
	
		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	int id = item.getItemId();
		
    	if (id == 1) {
			Intent profile = new Intent(getApplicationContext(),Profile.class);
			startActivity(profile);
			return true;
		}
		
		if(id == 2){
			session.logoutUser();
			return true;
		}
		
		if(id == 3)
		{
			Intent loginAct = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(loginAct);
			return true;
		}
		
		if(id == 4)
    	{
    		finish();
    		startActivity(getIntent());
    		return true;
    	}
		
		if (id == R.id.action_mail)
		{
			Intent msg = new Intent(getApplicationContext(),Message.class);
			startActivity(msg);
			return true;
		}
		
		if(id == R.id.ic_action_event)
		{
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(), CalendarActivity.class);
			startActivity(calendar);
		}
	
    	return super.onOptionsItemSelected(item);
    }

}