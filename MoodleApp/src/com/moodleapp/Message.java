package com.moodleapp;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.moodleapp.SubjectDetails.LazyAdapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class Message extends ListActivity {
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	// Session Manager Class
	SessionManager session;
	// to get user id and name
	String uid, name;
	// to get the menu item to store user's name
	MenuItem itemUser;
	private ProgressDialog pDialog;
	JSON_Parse jParser = new JSON_Parse();
	ArrayList<HashMap<String, String>> productsList;
	ArrayList<HashMap<String, String>> itemArray;
	private String getMessage = session.strUrl + "Message.php";
	private String sendMessage = session.strUrl + "InsertUnreadMessage.php";
	private String updateMessage = session.strUrl + "InsertMessage.php";
	JSONArray message = null;
	String contactId;
	String contactName;
	String contactBlocked;
	String sendchat;
	EditText txtSendMessage;
	ListView list;
	ListAdapter adapter;
	int success_read;
	int success_unread;
	boolean state;
	Bitmap proPicFrom;
	Bitmap proPicTo;
	Bitmap defProPic;
	private ActionBar actionBar;
	LazyAdapter msgCustomAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_layout);

		// object reference to session manager class
		session = new SessionManager(getApplicationContext());

		// check login
		// session.checkLogin();

		// get user data from session
		HashMap<String, String> user = session.getUserDetails();

		// store user id
		uid = user.get(SessionManager.KEY_USERID);

		// store username
		name = user.get(SessionManager.KEY_NAME);

		/*
		 * //message to indicate user's login status
		 * Toast.makeText(getApplicationContext(), "User Login Status: " +
		 * session.isLoggedIn(), Toast.LENGTH_LONG).show();
		 */

		Intent in = getIntent();

		actionBar = getActionBar();

		// Hide the action bar title
		actionBar.setDisplayHomeAsUpEnabled(true);

		// getting product id (pid) from intent
		// itemID = i.getStringExtra(TAG_PID);

		// contactName = in.getStringExtra("contactname");
		contactId = in.getStringExtra("contactid");
		contactBlocked = in.getStringExtra("contactblocked");

		productsList = new ArrayList<HashMap<String, String>>();

		// listV = ((ListView)findViewById(R.id.));

		// Loading products in Background Thread
		new GetMessage().execute();

		// Get listview
		/*
		 * ListView lv = getListView();
		 * 
		 * lv.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { // TODO Auto-generated method stub
		 * 
		 * String contactId =
		 * ((TextView)view.findViewById(R.id.id)).getText().toString(); String
		 * contactName = ((TextView)
		 * view.findViewById(R.id.name)).getText().toString(); String
		 * contactBlocked = ((TextView)
		 * view.findViewById(R.id.reference)).getText().toString();
		 * 
		 * Intent in = new Intent(getApplicationContext(), Message.class);
		 * 
		 * in.putExtra("contactname", contactName); in.putExtra("contactid",
		 * contactId); in.putExtra("contactblocked", contactBlocked);
		 * 
		 * startActivityForResult(in, 100); }
		 * 
		 * });
		 */

		txtSendMessage = (EditText) findViewById(R.id.txtEnterChat);

		/*
		 * txtSendMessage.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub new UpdateMessage().execute(); } });
		 */

		Button btnSend = (Button) findViewById(R.id.btnSend);
		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendchat = txtSendMessage.getText().toString();
				new SendMessage().execute();
			}
		});

		list = getListView();
		refreshList.start();
		state = true;
	}

	class GetMessage extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Message.this);
			// pDialog.setMessage("Loading Details. Please wait...");
			// pDialog.setIndeterminate(false);
			// pDialog.setCancelable(false);
			// pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userIdFrom", uid));
			params.add(new BasicNameValuePair("userIdTo", contactId));
			// getting JSON string from URL
			JSONObject json = jParser
					.makeHttpRequest(getMessage, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("GetMessage: ", json.toString());

			try {
				//msgCustomAdapter.clear();
				//msgCustomAdapter.notifyDataSetChanged();
				// Checking for SUCCESS TAG
				success_read = json.getInt("success_read");
				success_unread = json.getInt("success_unread");

				if (success_read == 1 || success_unread == 1) {
					message = json.getJSONArray("messages");
					// looping through All Products
					for (int i = 0; i < message.length(); i++) {
						JSONObject c = message.getJSONObject(i);

						// Storing each json item in variable
						String message = c.getString("message");
						String timeCreated = c.getString("timecreated");
						contactName = c.getString("name");
						String useridfrom = c.getString("useridfrom");
						String useridto = c.getString("useridto");

						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key =>
						// value
						map.put("details", contactName + " [" + timeCreated
								+ "]:");
						map.put("message", message);
						map.put("useridfrom", useridfrom);
						map.put("useridto", useridto);

						// map.put("userid", useridfrom);
						// map.put("timecreated", timeCreated);
						// map.put("timeread", timeRead);
						// if (useridfrom.equals("3")){
						// adding HashList to ArrayList
						productsList.add(map);
					}
				} else {
					// no products found
					// Launch Add New product Activity
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			proPicFrom = DownloadImage(session.strUrl + "moodle/user/pix.php/"
					+ uid + "/f1.jpg");
			proPicTo = DownloadImage(session.strUrl + "moodle/user/pix.php/"
					+ contactId + "/f1.jpg");
			//defProPic = BitmapFactory.decodeResource(getResources(),
					//R.drawable.profile);
			
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// lv
			TextView tv = (TextView) findViewById(R.id.lblCourses);
			tv.setText(contactName);

			runOnUiThread(new Runnable() {
				public void run() {
					msgCustomAdapter = new LazyAdapter(Message.this, productsList);
					list.setAdapter(msgCustomAdapter);
				}
			});
		}
	}

	private InputStream OpenHttpConnection(String urlString) throws IOException {
		InputStream in = null;
		int response = -1;

		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();

		if (!(conn instanceof HttpURLConnection))
			throw new IOException("Not an HTTP connection");

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			response = httpConn.getResponseCode();
			if (response == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			throw new IOException("Error connecting");
		}
		return in;
	}

	private Bitmap DownloadImage(String URL) {
		Bitmap bitmap = null;
		InputStream in = null;
		try {
			in = OpenHttpConnection(URL);
			bitmap = BitmapFactory.decodeStream(in);
			in.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return bitmap;
	}

	class SendMessage extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Message.this);
			// pDialog.setMessage("Loading Details. Please wait...");
			// pDialog.setIndeterminate(false);
			// pDialog.setCancelable(false);
			// pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userIdFrom", uid));
			params.add(new BasicNameValuePair("userIdTo", contactId));
			params.add(new BasicNameValuePair("message", sendchat));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(sendMessage, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("SendMessage: ", json.toString());

			/*
			 * try { // Checking for SUCCESS TAG int success =
			 * json.getInt("success");
			 * 
			 * if (success == 1) { // products found // Getting Array of
			 * Products message = json.getJSONArray("messages"); } else { // no
			 * products found // Launch Add New product Activity } } catch
			 * (JSONException e) { e.printStackTrace(); }
			 */

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					txtSendMessage.setText("");
					new GetMessage().execute();
					// HashMap<String, String> map = new HashMap<String,
					// String>();
					// map.put("EnteredMessage", sendchat);
					// itemArray.add(map);
					// ListAdapter adapter = new SimpleAdapter(
					// Message.this, itemArray,
					// R.layout.message_from_layout,
					// new String[] { "message", "userid" },
					// new int[] { R.id.messageFrom, R.id.fromid });
					// setListAdapter(adapter);

				}
			});

		}
	}

	class UpdateMessage extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Message.this);
			// pDialog.setMessage("Loading Details. Please wait...");
			// pDialog.setIndeterminate(false);
			// pDialog.setCancelable(false);
			// pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userIdFrom", uid));
			params.add(new BasicNameValuePair("userIdTo", contactId));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(updateMessage, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("UpdateMessage: ", json.toString());

			/*
			 * try { // Checking for SUCCESS TAG int success =
			 * json.getInt("success");
			 * 
			 * if (success == 1) { // products found // Getting Array of
			 * Products message = json.getJSONArray("messages"); } else { // no
			 * products found // Launch Add New product Activity } } catch
			 * (JSONException e) { e.printStackTrace(); }
			 */

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					new GetMessage().execute();
				}
			});

		}
	}
	
	class LazyAdapter extends BaseAdapter {

		private Activity activity;
		private ArrayList<HashMap<String, String>> data;
		private LayoutInflater inflater = null;

		// public ImageLoader imageLoader;

		public LazyAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
			activity = a;
			data = d;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// imageLoader=new ImageLoader(activity.getApplicationContext());
		}

		public int getCount() {
			return data.size();
		}
		
		public void clear()
		{ 
			data.clear(); 
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			//if(convertView==null)
			// try{
			HashMap<String, String> song = new HashMap<String, String>();
			song = data.get(position);
			//int cout = 0;

			if (song.get("useridfrom").equals(uid) || song.get("useridto").equals(contactId)) {
				//if (convertView == null)
					vi = inflater.inflate(R.layout.message_from_layout, null);

				TextView messageFromDate = (TextView) vi.findViewById(R.id.messageFromDate); // title
				TextView messageFrom = (TextView) vi.findViewById(R.id.messageFrom);
				//ImageView proPocFrom = (ImageView) findViewById(R.id.profilePicFrom);

				// Setting all values in listview
				messageFromDate.setText(song.get("details"));
				messageFrom.setText(song.get("message"));
				//String imag_url = "http://10.0.2.2/moodle/user/pix.php/" + uid + "/f1.jpg";
				//if (song.get("useridfrom").equals(uid))
				//{
				//if (proPicFrom != null)
				//	proPocFrom.setImageBitmap(proPicFrom);
				//else
			//		proPocFrom.setImageBitmap(defProPic);
				//}
				
				//if (song.get("useridto").equals(contactId))
				//{
				//	proPocFrom.setImageBitmap(pro);
				//}
				//cout++;
			}
			//if (song.get("useridfrom").equals(contactId) || song.get("useridto").equals(uid)) {
				//if (convertView == null)
			else{
					vi = inflater.inflate(R.layout.message_to_layout, null);

				TextView messageToDate = (TextView) vi.findViewById(R.id.messageToDate); // title
				TextView messageTo = (TextView) vi.findViewById(R.id.messageTo);
				//ImageView proPocTo = (ImageView) findViewById(R.id.profilePicTo);

				// Setting all values in listview
				messageToDate.setText(song.get("details"));
				messageTo.setText(song.get("message"));
				//if (proPicTo != null)
				//	proPocTo.setImageBitmap(proPicTo);
				//else
				//	proPocTo.setImageBitmap(defProPic);
			}

			// } catch (Exception e) {
			// Log.e("List: ", e.getMessage());
			// }
			return vi;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.icon_menu, menu);

		if (session.isLoggedIn() == true) {
			menu.add(0, 1, Menu.NONE, name);
			menu.add(0, 4, Menu.NONE, "Refresh");
			menu.add(0, 2, Menu.NONE, "Logout");
			getMenuInflater().inflate(R.menu.logged_in, menu);
		} else {
			menu.add(0, 3, Menu.NONE, "Login");
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(),
					MainTabs.class);
			startActivity(profile);
			return true;
		}

		if (id == 1) {
			Intent profile = new Intent(getApplicationContext(), Profile.class);
			startActivity(profile);
			return true;
		}

		if (id == 2) {
			session.logoutUser();
			return true;
		}

		if (id == 3) {
			Intent loginAct = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(loginAct);
			return true;
		}

		if (id == 4) {
			finish();
			startActivity(getIntent());
			return true;
		}

		if (id == R.id.action_mail) {
			Intent msg = new Intent(getApplicationContext(), Message.class);
			startActivity(msg);
			return true;
		}

		if (id == R.id.ic_action_event) {
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null,
				//	CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(),CalendarActivity.class);
			startActivity(calendar);
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onRestart() {
		super.onRestart();
		finish();
		startActivity(getIntent());
	}

	Handler handler = new Handler();

	final Runnable r = new Runnable() {
		public void run() {
			new UpdateMessage().execute();
			// handler.postDelayed(this, 10000);
		}
	};

	Thread refreshList = new Thread() {
		@Override
		public void run() {
			try {
				while (true) {
					sleep(15000);
					handler.post(r);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onDestroy() {
		// state = false;
		refreshList.interrupt();
		super.onDestroy();
	}
}
