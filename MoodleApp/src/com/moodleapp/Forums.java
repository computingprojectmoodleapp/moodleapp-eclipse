package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class Forums extends ListActivity {
	String subjectID;
	String forumID;
	String topic;
	
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	
	private ProgressDialog dialog;
	
	ArrayList<HashMap<String, String>> topicList;
	
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forum_view);
		
		Intent intent = getIntent();
		//getting courseid from the previous intent
		subjectID = intent.getStringExtra("id");
		
		topicList = new ArrayList<HashMap<String,String>>();
		
		//loading topics in background thread
		new LoadForumTopics().execute();
		
		ListView listview = getListView();
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent intent = new Intent(getApplicationContext(), DiscussionTopics.class);
				
				intent.putExtra("id", subjectID);
				intent.putExtra("forumID", forumID);
				startActivityForResult(intent, 100);
			}
			
		});
	}
	
	class LoadForumTopics extends AsyncTask<String, String, String>{
		//show process dialog
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			dialog = new ProgressDialog(Forums.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		//getting all topics from URL
		protected String doInBackground(String...args){
			//building params
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("course", subjectID));
			
			//getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "getforumtopic.php", "GET", params);
			
			//check log cat for JSON response
			Log.d("topics: ", json.toString());
			
			try{
				jArray = json.getJSONArray("topics");
				//looping through the JSONArray
				for(int i=0; i<jArray.length(); i++){
					JSONObject t = jArray.getJSONObject(i);
					
					//storing each JSON item in variables
					forumID = t.getString("id");
					topic = t.getString("intro");
					
					//creating a HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					
					//adding each child node to HashMap
					map.put("course", subjectID);
					map.put("id", forumID);
					map.put("topic", topic);
					
					//adding the HasMap to ArrayList
					topicList.add(map);
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// dismiss Progress Dialog after all topics are loaded
			dialog.dismiss();
			
			TextView label = (TextView) findViewById(R.id.lblForum);
			label.setText("Forums");

			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					//updating JSON data into a ListView
					ListAdapter adapter = new SimpleAdapter(Forums.this, topicList, R.layout.forum_type_item, new String[] {"courseid", "topic"}, new int[] {R.id.courseid, R.id.forum_topic});
					setListAdapter(adapter);
					
				}
			});
		}
	}
}
