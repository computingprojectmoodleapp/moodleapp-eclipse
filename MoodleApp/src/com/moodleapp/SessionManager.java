package com.moodleapp;

import java.util.HashMap;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

@SuppressLint("CommitPrefEdits")
public class SessionManager {
	//unique ip
	public final static String strUrl = "http://moodleapp.pixub.com/PHP/";
	public final static String moodleRoot = "http://moodleapp.pixub.com/";
	// Shared Preferences
		SharedPreferences pref;
		
		// Editor for Shared preferences
		Editor editor;
		
		// Context
		Context _context;
		
		// Shared pref mode
		int PRIVATE_MODE = 0;
		
		// Sharedpref file name
		private static final String PREF_NAME = "AndroidHivePref";
		
		// All Shared Preferences Keys
		private static final String IS_LOGIN = "IsLoggedIn";
		
		// Userid
		public static final String KEY_USERID = "userid";
		
		//name
		public static final String KEY_NAME = "name";
		
		//role
		public static final String KEY_ROLE = "role";
		
		// Constructor
		public SessionManager(Context context){
			this._context = context;
			pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
			editor = pref.edit();
		}
		
		//create Login sessions
		public void createLoginSession(String uid, String name, String role){
			
			// Storing login value as TRUE
			editor.putBoolean(IS_LOGIN, true);
			
			// Storing name in pref
			editor.putString(KEY_USERID, uid);
			
			// Storing email in pref
			editor.putString(KEY_NAME, name);

			editor.putString(KEY_ROLE, role);
			// commit changes
			editor.commit();
		}	
		
		/**
		 * Check login method wil check user login status
		 * If false it will redirect user to login page
		 * Else won't do anything
		 * */
		/*public void checkLogin(){
			// Check login status
			if(!this.isLoggedIn()){
				// user is not logged in redirect him to Login Activity
				Intent i = new Intent(_context, MainTabs.class);
				// Closing all the Activities
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				// Add new Flag to start new Activity
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				
				// Staring Login Activity
				_context.startActivity(i);
			}
			
		}*/
		
		//get stored session details
		public HashMap<String, String> getUserDetails(){
			
			HashMap<String, String> user = new HashMap<String, String>();
			
			//user's name
			user.put(KEY_NAME, pref.getString(KEY_NAME, null));
			
			//user id
			user.put(KEY_USERID, pref.getString(KEY_USERID, null));
			
			//user's role
			user.put(KEY_ROLE, pref.getString(KEY_ROLE,null));
			
			// return user
			return user;
		}
		
		//Clear Sessions
		public void logoutUser(){
			// Clearing all data from Shared Preferences
			editor.clear();
			editor.commit();
			
			// After logout redirect user to Loing Activity
			Intent i = new Intent(_context, MainTabs.class);
			
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			// Staring Login Activity
			_context.startActivity(i);
		}
		
		// Get Login State
		public boolean isLoggedIn(){
			return pref.getBoolean(IS_LOGIN, false);
		}
}
