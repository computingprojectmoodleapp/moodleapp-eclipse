package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ForumPosts extends ListActivity {
	
	String forumtopic;
	String topic;
	String discussionID;
	String creator;
	String startDate;
	String message;
	String postID;
	String pid;
	String ftopic;
	String user;
	String poster;
	String courseid;
	String userid;
	String posterid;
	String creatorid;
	
	AlertDialogManager alert = new AlertDialogManager();
	int result;
	String msg;
	
	private ProgressDialog dialog;
	
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	ArrayList<HashMap<String, String>> forumpost;
	
	SessionManager session;
	String role;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forum_post_view);
		//text = (TextView) findViewById(R.id.forumsubject);
		
		Intent intent = getIntent();
		//getting courseID and forumID from previous intent
		discussionID = intent.getStringExtra("id");
		forumtopic = intent.getStringExtra("forumtopic");
		courseid = intent.getStringExtra("courseid");
		
		//getting user's role and user ID
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		role = user.get(SessionManager.KEY_ROLE);
		userid = user.get(SessionManager.KEY_USERID);
		
		forumpost = new ArrayList<HashMap<String,String>>();
		
		//loading the post contents in a background thread
		new LoadForumPost().execute();
		
		//context menu
		ListView list = getListView();
		registerForContextMenu(list);
		
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				pid = ((TextView)view.findViewById(R.id.courseid)).getText().toString();
				creatorid = ((TextView)view.findViewById(R.id.posterid)).getText().toString();
				return false;
			}
		});
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		menu.setHeaderTitle("Forums");	
		menu.add(Menu.NONE, 2, 2, "Delete");
		menu.add(Menu.NONE, 1, 1, "Reply");
	}
	
	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		
		if(item.getItemId() == 1){
			Intent reply = new Intent(getApplicationContext(), ReplyPost.class);
			reply.putExtra("discussionid", discussionID);
			reply.putExtra("parent", pid);
			reply.putExtra("ftopic", topic);
			reply.putExtra("courseid", courseid);
			
			startActivityForResult(reply, 100);
		}
		if (item.getItemId() == 2){
			creatorid = ((TextView)findViewById(R.id.posterid)).getText().toString();
		
			new AlertDialog.Builder(this).setTitle("Confirm Delete")
			.setMessage("Delete this forum post?")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int i) {
					
					
					if(!userid.equals(posterid)){
							alert.showAlertDialog(ForumPosts.this, "Authentication Error!", "You are only allowed to delete your own posts." , false);
					}
					else {
					AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
					pid = ((TextView)info.targetView.findViewById(R.id.courseid)).getText().toString();
					new DeletePost().execute();
					}
				}
			})
			.setNeutralButton("Cancel", null)
			.create()
			.show();
	
		}
		return true;
	}
	
	
	class LoadForumPost extends AsyncTask<String, String, String>{
		//show Progress Dialog
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(ForumPosts.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		//getting the forum post content from given URL
		protected String doInBackground(String...args){
			//Building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("did", discussionID));
			params.add(new BasicNameValuePair("userid", userid));
			params.add(new BasicNameValuePair("courseid", courseid));
			
			//getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "getforumpost.php", "GET", params);
			//check log for JSON response
			Log.d("content: ", json.toString());
			
			try{
				jArray = json.getJSONArray("content");
				
				//traversing through the JSONArray
				for(int i=0; i<jArray.length(); i++){
					JSONObject post = jArray.getJSONObject(i);
					
					//storing each JSON item in variables
					postID = post.getString("id");
					topic = post.getString("subject");
					message = post.getString("message");
					startDate = post.getString("lastmod");
					posterid = post.getString("userid");
					user = post.getString("username");
					poster = post.getString("role");
					
					//creating a HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					
					//adding each variable to HashMap
					map.put("postID", postID);
					map.put("subject", topic);
					map.put("message", message);
					map.put("lastmodified", startDate);
					map.put("posterid", posterid);
					map.put("user", user);
					map.put("poster", poster);
					
					//adding the HashMap to ArrayList
					forumpost.add(map);
					
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			
			TextView top = (TextView) findViewById(R.id.lblForum);
			top.setText(forumtopic);	
			top.setTextColor(getResources().getColor(R.color.red));
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					//updating JSONData into a ListView
					ListAdapter adapter = new SimpleAdapter(ForumPosts.this, forumpost, R.layout.forum_post, new String[] {"postID", "subject", "message", "lastmodified", "user", "poster", "posterid"}, new int[] {R.id.courseid, R.id.forumsubject, R.id.forum_content, R.id.posted_date, R.id.creator, R.id.role, R.id.posterid });
					setListAdapter(adapter);
					
					//setContentView(R.layout.forum_post);
					LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View myView = inflater.inflate(R.layout.forum_post, null);
					TextView txt = (TextView) myView.findViewById(R.id.forum_content);
					txt.setTextColor(getResources().getColor(R.color.red));
				}
			});
		}
	}
	
	class DeletePost extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(ForumPosts.this);
			dialog.setMessage("Deleting...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("id", postID));
			
			//getting SQL query to work via JSON
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "deletepost.php", "GET", params);
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				msg = json.getString("message");
				
			}catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String filepath) {
			//dismiss the progress dialog
			dialog.dismiss();
			
			if(result == 1){
				Toast.makeText(ForumPosts.this, msg, Toast.LENGTH_LONG).show();
				finish();
				startActivity(getIntent());
			}
			if(result == 0){
				Toast.makeText(ForumPosts.this, msg, Toast.LENGTH_LONG).show();
			}
		}
	}
	
}
