package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DiscussionTopics extends ListActivity {
	
	String subjectID;
	String forumID;
	String topic;
	String discussionID;
	String creator;
	String startDate;
	String message;
	String pid;
	
	AlertDialogManager alert = new AlertDialogManager();
	int result;
	String msg;
	
	private ProgressDialog dialog;
	
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	
	ArrayList<HashMap<String, String>> topicsList;
	
	SessionManager session;
	String role;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.discussion_topic_view);
		
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		
		//get User's role
		role = user.get(SessionManager.KEY_ROLE);
		
		Intent intent = getIntent();
		//getting courseID and forumID from previous intent
		subjectID = intent.getStringExtra("id");
		forumID = intent.getStringExtra("forumID");
		
		topicsList = new ArrayList<HashMap<String,String>>();
		
		//loading discussion topics in background thread
		new LoadDiscussionTopics().execute();
		
		ListView listview = getListView();
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				String discussion  = ((TextView)view.findViewById(R.id.courseid)).getText().toString();
				String forum_topic = ((TextView)view.findViewById(R.id.forum_topic)).getText().toString();
				
				Intent intent = new Intent(getApplicationContext(), ForumPosts.class);
				
				intent.putExtra("id", discussion);
				intent.putExtra("forumtopic", forum_topic);
				intent.putExtra("courseid", subjectID);
				startActivityForResult(intent, 100);
			}
			
		});
		
		ListView list = getListView();
		registerForContextMenu(list);
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				pid = ((TextView)view.findViewById(R.id.courseid)).getText().toString();
				
				return false;
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.forum_menu, menu);
		
		if(!role.equals("Teacher")){
			menu.setGroupVisible(Menu.NONE, false);
		}
		return true;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if(role.equals("Teacher")){
			menu.setHeaderTitle("Options");
			menu.add(Menu.NONE, 1, 1, "Delete Topic");
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id == R.id.action_new_forum){
			Intent addPost = new Intent(getApplicationContext(), AddForumPost.class);
			addPost.putExtra("id", subjectID);
			addPost.putExtra("forumID", forumID);
			
			startActivityForResult(addPost, 100);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		if(item.getItemId() == 1){
			new AlertDialog.Builder(this).setTitle("Confirm Delete")
			.setMessage("Delete this discussion topic?")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
					pid = ((TextView)info.targetView.findViewById(R.id.courseid)).getText().toString();
					new DeleteForum().execute();
				}
			})
			.setNeutralButton("Cancel", null)
			.create()
			.show();
		}
		return true;
	}
	
	class LoadDiscussionTopics extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(DiscussionTopics.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		//getting discussion topics from given URL
		protected String doInBackground(String...args){
			//Building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("course", subjectID));
			params.add(new BasicNameValuePair("forum", forumID));
			
			//getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "getdiscussiontopics.php", "GET", params);
			
			//check log for JSON response
			Log.d("Topics: ", json.toString());
			
			try{
				jArray = json.getJSONArray("result");
				
				//traversing through the JSONArray
				for(int i=0; i<jArray.length(); i++){
					JSONObject t = jArray.getJSONObject(i);
					
					//storing each value in variables
					discussionID = t.getString("id");
					topic = t.getString("name");
					startDate = t.getString("date");
					creator = t.getString("username");
					
					//creating a HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					
					//adding each child node to HashMap
					map.put("id", discussionID);
					map.put("topic", topic);
					map.put("creator", creator);
					map.put("startDate", startDate);
					
					//adding the HashMap to ArrayList
					topicsList.add(map);
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			//dismiss dialog after loading is complete
			dialog.dismiss();
			
			TextView label = (TextView) findViewById(R.id.lblForum);
			label.setText("News Forum");
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					//updating JSON data into a ListView
					ListAdapter adapter = new SimpleAdapter(DiscussionTopics.this, topicsList, R.layout.discussion_topic_layout, new String[] {"id","topic","creator","startDate"}, new int[] {R.id.courseid,R.id.forum_topic,R.id.created_by,R.id.created_date});
					setListAdapter(adapter);
				}
			});
		}
	}
	
	class DeleteForum extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(DiscussionTopics.this);
			dialog.setMessage("Deleting...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("id", discussionID));
			
			//getting SQL query to work via JSON
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "deleteforum.php", "GET", params);
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				msg = json.getString("message");
				
			}catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String filepath) {
			//dismiss progress dialog after deleting
			dialog.dismiss();
			
			if(result == 1){
				Toast.makeText(DiscussionTopics.this, msg, Toast.LENGTH_LONG).show();
				finish();
				startActivity(getIntent());
			}
			
			if(result == 0){
				Toast.makeText(DiscussionTopics.this, msg, Toast.LENGTH_LONG).show();
			}
		}
		
	}
}
