package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.text.Html;
import hu.scythe.droidwriter.DroidWriterEditText;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCourseNotice extends Activity {
	
	String subjectID;
	String coursename;
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	EditText txtSubject;
	AlertDialogManager alert = new AlertDialogManager();
	private DroidWriterEditText droidWriterEditText;
	private Button btnClear;
	
	SessionManager session;
	String userID;
	
	String subject;
	String content;
	int result;
	
	String msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_course_notice);
		
		Intent intent = getIntent();
		//getting the courseid from the previous intent
		subjectID = intent.getStringExtra("id");
		coursename = intent.getStringExtra("name");
		
		//getting userid from sessions
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		
		userID = user.get(SessionManager.KEY_USERID);
		 
		txtSubject = (EditText) findViewById(R.id.editHeader);

		//initializing droidWriterEditText, a customized third party EditText
		droidWriterEditText = (DroidWriterEditText) findViewById(R.id.notice);

		//setting up toggle button for BOLD
		droidWriterEditText.setBoldToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleBold));

		//setting up toggle button for ITALIC
		droidWriterEditText.setItalicsToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleItalic));

		//setting up toggle button for UNDERLINE
		droidWriterEditText.setUnderlineToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleUnderline));

		//setting up button for clear text
		droidWriterEditText.setClearButton(btnClear = (Button) findViewById(R.id.btnClear));

		Button btnAdd = (Button) findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				subject = txtSubject.getText().toString();
				//retrieve text in droidWriterEditText as a html and assign it to field variable content
				content = Html.toHtml(droidWriterEditText.getSpannedText());
				if((subject.trim().isEmpty()) || (content.trim().isEmpty())){
					alert.showAlertDialog(AddCourseNotice.this, "Error!", "Please fill all fields.", false);
				}
				else{
					//calling the background thread to execute
					//this object calls to webService which is programmed to add course notice to remote centralized database
					new AddNotice().execute();
				}
				
			}
		});

		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//clear edited texts
				clearTexts();
			}
		});
	
	}
	
	public void clearTexts(){
		txtSubject.setText("");
		droidWriterEditText.setText("");
	}
	
	class AddNotice extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
		
		protected String doInBackground(String...args){
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userid", userID));
			params.add(new BasicNameValuePair("courseid", subjectID));
			params.add(new BasicNameValuePair("subject", subject));
			params.add(new BasicNameValuePair("content", content));
			
			Log.d("doInbackground", "working");
			
			//getting JSON string URL
			//calls to webService which is programmed to add course notice to remote centralized database
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "addcoursenotices.php", "GET", params);
			
			Log.d("getting json object", "working");
			
			//check log cat for JSON response
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				Log.d("getting success", "working");
				
				msg = json.getString("message");
				
			}catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String filepath){
		
			if (result == 1){
				Toast.makeText(AddCourseNotice.this, msg, Toast.LENGTH_SHORT).show();
				clearTexts();
				finish();
				
				Intent intent = new Intent(getApplicationContext(), CourseNotices.class);
				intent.putExtra("id", subjectID);
				intent.putExtra("name", coursename);
				startActivityForResult(intent, 100);
			}
			if (result == 0) {
				alert.showAlertDialog(AddCourseNotice.this, "Oops!", msg, false);
			}
			
		}
	}
		
	}
	
