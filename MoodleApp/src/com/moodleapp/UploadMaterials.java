package com.moodleapp;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import ar.com.daidalos.afiledialog.FileChooserDialog;

public class UploadMaterials extends Activity {
	
	SessionManager session;

	String uploadFilePath;
	String uploadFileName;

	boolean statusUpload = false;
	boolean statusUpdate = false;

	// String fileName;

	public static final String[] MONTHS = { "January", "February", "March",
			"April", "May", "June", "July", "August", "September", "October",
			"November", "December" };

	int serverResponseCode = 0;

	JSON_Parse jParser = new JSON_Parse();

	String upLoadServerUri = session.strUrl + "UploadFiles.php";

	String subjectCreatedDate = session.strUrl + "GetSubjectCreatedDate.php";

	String updateUploadTables = session.strUrl + "UploadCourseMaterialsTableUpdates.php";

	String startdate;
	String numsections;

	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat format = new SimpleDateFormat("MM-dd");
	Date ndate = null;
	Date pdate = null;

	ArrayList<String> years;
	ArrayAdapter<String> adapter;

	Spinner spinYear;

	String subjectID;
	String name;
	String reference;
	String summary;
	String section;
	long tes;
	String visibility;

	private ProgressDialog pDialog;

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_materials_layout);

		spinYear = (Spinner) findViewById(R.id.spWeek);
		// spinYear.setBackgroundColor(color.black);
		// TextView tv = (TextView) spinYear.getSelectedView();
		// tv.setTextColor(Color.BLACK);

		new SubjectStartDate().execute();

		Intent i = getIntent();

		// getting product id (pid) from intent
		subjectID = i.getStringExtra("id");
		// itemName = i.getStringExtra(TAG_NAME);

		Button btnBrowseFile = (Button) findViewById(R.id.btnBrowseFile);
		btnBrowseFile.setOnClickListener(btnDialogSimpleOpen);

		Button btnUpload = (Button) findViewById(R.id.btnUploadFile);
		btnUpload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText txtName = (EditText) findViewById(R.id.txtName);
				name = txtName.getText().toString();
				// section =
				// adapter.getItem(spinYear.getSelectedItemPosition());
				int pos = spinYear.getSelectedItemPosition() - 1;
				section = "" + pos;
				EditText txtSummery = (EditText) findViewById(R.id.txtSummery);
				summary = txtSummery.getText().toString();
				new UploadCourseMaterials().execute();

				// Toast toast = Toast.makeText(UploadMaterials.this,
				// section, Toast.LENGTH_LONG);
				// toast.show();
			}
		});

		CheckBox cbVisibile = (CheckBox) findViewById(R.id.cbVisible);
		cbVisibile.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					visibility = "1";
				} else {
					visibility = "0";
				}
			}
		});

	}

	private String getMonthString(int monthInt) {
		String monthStr = MONTHS[monthInt];
		return monthStr;
	}

	private OnClickListener btnDialogSimpleOpen = new OnClickListener() {
		public void onClick(View v) {
			// Create the dialog.
			FileChooserDialog dialog = new FileChooserDialog(
					UploadMaterials.this);

			// Assign listener for the select event.
			dialog.addListener(UploadMaterials.this.onFileSelectedListener);

			// Show the dialog.
			dialog.show();
		}
	};

	private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
		public void onFileSelected(Dialog source, File file) {
			source.hide();
			TextView txtFileName = (TextView) findViewById(R.id.lblFilePath);
			txtFileName.setText(file.getName());
			uploadFileName = file.getName();
			uploadFilePath = file.getAbsolutePath();
			reference = file.getName();
			// Toast toast = Toast.makeText(UploadMaterials.this,
			// "File selected: " + file.getName(), Toast.LENGTH_LONG);
			// toast.show();
		}

		public void onFileSelected(Dialog source, File folder, String name) {
			source.hide();

		}
	};

	class SubjectStartDate extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(UploadMaterials.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("subjectID", subjectID));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(subjectCreatedDate,
					"GET", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt("success");

				if (success == 1) {
					// products found
					// Getting Array of Products
					startdate = json.getString("startdate");
					numsections = json.getString("numsections");

					// looping through All Products

				} else {
					// no products found
					// Launch Add New product Activity
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();

			runOnUiThread(new Runnable() {
				public void run() {

					try {
						ndate = format.parse(startdate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						Log.e("Date", "Date error");
						e.printStackTrace();
					}

					years = new ArrayList<String>();
					years.add("Select Week");
					years.add("Subject Header");
					for (int i = 1; i <= Integer.parseInt(numsections); i++) {
						pdate = ndate;
						Calendar cal = Calendar.getInstance();
						cal.setTime(ndate);
						cal.add(Calendar.DATE, 6);
						ndate = cal.getTime();
						years.add(getMonthString(pdate.getMonth()) + " "
								+ pdate.getDate() + " - "
								+ getMonthString(ndate.getMonth()) + " "
								+ ndate.getDate());
						cal.setTime(ndate);
						cal.add(Calendar.DATE, 1);
						ndate = cal.getTime();
					}
					adapter = new ArrayAdapter<String>(getApplicationContext(),
							R.layout.week_spinner_item, years);
					adapter.setDropDownViewResource(R.layout.week_spinner_selected_item);
					spinYear.setAdapter(adapter);
					spinYear.setSelection(0);
				}
			});
		}
	}

	class UploadCourseMaterials extends AsyncTask<String, String, String> {

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(uploadFilePath);
		private FileInputStream fileInputStream;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(UploadMaterials.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/*
		 * protected void onPreExecute() { super.onPreExecute(); pDialog = new
		 * ProgressDialog(UploadMaterials.this);
		 * pDialog.setMessage("Downloading file. Please wait...");
		 * pDialog.setButton("OK", new DialogInterface.OnClickListener() {
		 * public void onClick(DialogInterface dialog, int which) {
		 * ((AlertDialog) dialog).getButton(which).setVisibility(
		 * View.INVISIBLE); } }); pDialog.setIndeterminate(false);
		 * pDialog.setMax(100);
		 * pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		 * pDialog.setCancelable(false); pDialog.show(); }
		 */

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			// open a URL connection to the Servlet
			try {
				fileInputStream = new FileInputStream(sourceFile);
				URL url = new URL(upLoadServerUri + "?subjectID=" + subjectID + "&type=14");

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", uploadFilePath);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=uploaded_file;filename="
						+ uploadFilePath + "" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);
			} catch (Exception e) {

			}
			return null;
		}

		/**
		 * Updating progress bar
		 * */
		/*
		 * protected void onProgressUpdate(String... progress) { // setting
		 * progress percentage
		 * pDialog.setProgress(Integer.parseInt(progress[0])); }
		 */

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();
			statusUpload = true;
			new UploadTableUpdate().execute();
		}

		class UploadTableUpdate extends AsyncTask<String, String, String> {

			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				pDialog = new ProgressDialog(UploadMaterials.this);
				pDialog.setMessage("Loading Details. Please wait...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
			}

			/**
			 * getting All products from url
			 * */
			protected String doInBackground(String... args) {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("subjectID", subjectID));
				params.add(new BasicNameValuePair("name", name));
				params.add(new BasicNameValuePair("reference", reference));
				params.add(new BasicNameValuePair("summary", summary));
				params.add(new BasicNameValuePair("section", section));
				params.add(new BasicNameValuePair("visible", visibility));
				// getting JSON string from URL
				JSONObject json = jParser.makeHttpRequest(updateUploadTables,
						"GET", params);

				// Check your log cat for JSON reponse
				Log.d("All Products: ", json.toString());

				try {
					// Checking for SUCCESS TAG
					int success = json.getInt("success");

					if (success == 1) {
						// products found
						// Getting Array of Products
						startdate = json.getString("startdate");
						numsections = json.getString("numsections");

						// looping through All Products

					} else {
						// no products found
						// Launch Add New product Activity
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				return null;
			}

			protected void onPostExecute(String file_url) {
				// dismiss the dialog after getting all products
				pDialog.dismiss();
				statusUpdate = true;
				if (statusUpdate && statusUpload) {

					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							UploadMaterials.this);
					dlgAlert.setMessage(uploadFileName
							+ " Uploaded Successfully");
					dlgAlert.setTitle("Upload Complete");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(false);
					dlgAlert.create().show();
					dlgAlert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
								}
							});

					// Displaying downloaded image into image view
					// Reading image path from sdcard
					// String imagePath =
					// Environment.getExternalStorageDirectory().toString() +
					// "/downloadedfile";
					// setting downloaded into image view
					// my_image.setImageDrawable(Drawable.createFromPath(imagePath));
				} else {
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							UploadMaterials.this);
					dlgAlert.setMessage(uploadFileName + " Cannot be Uploaded");
					dlgAlert.setTitle("Upload Error");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(false);
					dlgAlert.create().show();
					dlgAlert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
								}
							});
				}
			}
		}
	}

}
