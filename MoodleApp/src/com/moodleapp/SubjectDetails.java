package com.moodleapp;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SubjectDetails extends ListActivity {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;

	// to get user id and name
	String uid, name;

	// to get the menu item to store user's name
	MenuItem itemUser;

	String subjectID;

	String itemName;

	private ProgressDialog pDialog0;
	private ProgressDialog pDialog1;
	private ProgressDialog pDialog2;

	JSON_Parse jParser = new JSON_Parse();
	private ActionBar actionBar;

	ArrayList<HashMap<String, String>> productsList;

	private String url_all_products = session.strUrl + "Assignments&Resources.php";
	private String file_urlLink = session.strUrl + "DownloadFileDetails.php";
	// private static String file_urlDownload;

	// private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCTS = "resources";

	public static final int progress_bar_type = 0;

	JSONArray resources = null;
	JSONArray link = null;

	String item_ID;
	String item_Name;
	String item_Module;
	String item_Instance;

	LazyAdapter adapter;

	String dwnLink;

	String filename;
	String role;

	ListView list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.subject_details_layout);

		// object reference to session manager class
		session = new SessionManager(getApplicationContext());

		// check login
		// session.checkLogin();

		// get user data from session
		HashMap<String, String> user = session.getUserDetails();

		// store user id
		uid = user.get(SessionManager.KEY_USERID);

		// store username
		name = user.get(SessionManager.KEY_NAME);

		role = user.get(SessionManager.KEY_ROLE);
		/*
		 * //message to indicate user's login status
		 * Toast.makeText(getApplicationContext(), "User Login Status: " +
		 * session.isLoggedIn(), Toast.LENGTH_LONG).show();
		 */

		Intent i = getIntent();

		// getting product id (pid) from intent
		subjectID = i.getStringExtra("id");
		itemName = i.getStringExtra("name");

		productsList = new ArrayList<HashMap<String, String>>();

		// Loading products in Background Thread
		new LoadAllProducts().execute();

		actionBar = getActionBar();

		// Hide the action bar title
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Get listview
		list = getListView();

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				// item_ID = ((TextView) view.findViewById(R.id.id)).getText()
				// .toString();
				item_Name = ((TextView) view.findViewById(R.id.name)).getText()
						.toString();
				item_Module = ((TextView) view.findViewById(R.id.module))
						.getText().toString();
				item_Instance = ((TextView) view.findViewById(R.id.instance))
						.getText().toString();

				if (!item_Module.equals("0")) {

					if (item_Module.equals("1")) {
						Intent in = new Intent(getApplicationContext(),
								UploadAssignments.class);
						in.putExtra("id", subjectID);
						in.putExtra("name", item_Name);
						in.putExtra("module", item_Module);
						in.putExtra("instance", item_Instance);
						startActivityForResult(in, 100);
					} else if (item_Module.equals("14")) {
						new GetDownloadLink().execute();
					}
				}
			}

		});
	}

	class LoadAllProducts extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog0 = new ProgressDialog(SubjectDetails.this);
			pDialog0.setMessage("Loading Details. Please wait...");
			pDialog0.setIndeterminate(false);
			pDialog0.setCancelable(false);
			pDialog0.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("subjectID", subjectID));
			params.add(new BasicNameValuePair("userid", uid));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_products, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				// int success = json.getInt(TAG_SUCCESS);

				if (json.getInt("success_mr") == 1
						|| json.getInt("success_as") == 1) {
					// products found
					// Getting Array of Products
					resources = json.getJSONArray(TAG_PRODUCTS);

					HashMap<String, String> map;

					for (int sectionNum = 0; sectionNum <=
						Integer.parseInt(json
						.getString("numberofsec")); sectionNum++) {

					//for (int sectionNum = 0; sectionNum < 10; sectionNum++) {

						if (sectionNum == 0) {
							map = new HashMap<String, String>();
							map.put("id", "-1");
							map.put("module", "0");
							map.put("section", "-1");
							map.put("instance", "-1");
							map.put("name", "Header");
							productsList.add(map);
						} else {
							map = new HashMap<String, String>();
							map.put("id", "-1");
							map.put("module", "0");
							map.put("section", "-1");
							map.put("instance", "-1");
							map.put("name", "Week " + sectionNum);
							productsList.add(map);
						}

						// looping through All Products
						for (int i = 0; i < resources.length(); i++) {
							JSONObject c = resources.getJSONObject(i);

							// Storing each json item in variable
							String id = c.getString("id");
							String module = c.getString("module");
							String section = c.getString("section");
							String sectionid = c.getString("sectionid");
							String instance = c.getString("instance");
							String name = c.getString("name");

							// if (sectionid != null) {

							if (sectionNum == Integer.parseInt(sectionid)) {
								// creating new HashMap
								map = new HashMap<String, String>();

								/*
								 * if (i == 0) { map.put(TAG_PID, "-1");
								 * map.put("module", "0"); map.put("section",
								 * "-1"); map.put("instance", "-1");
								 * map.put(TAG_NAME, "Header"); } else { //
								 * adding each child node to HashMap key =>
								 * value if ()
								 */
								map.put("id", id);
								map.put("module", module);
								map.put("section", section);
								map.put("instance", instance);
								map.put("name", name);

								// adding HashList to ArrayList
								productsList.add(map);
							}
							// }
						}
					}
				} else {
					// no products found
					// Launch Add New product Activity
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog0.dismiss();

			TextView tv = (TextView) findViewById(R.id.lblCourses);
			tv.setText(itemName);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					/*
					 * ListAdapter adapter = new SimpleAdapter(
					 * MainActivity.this, productsList, R.layout.list_item, new
					 * String[] { TAG_PID, TAG_NAME, "module", "instance" }, new
					 * int[] { R.id.id, R.id.name, R.id.module, R.id.instance
					 * }); // updating listview setListAdapter(adapter);
					 */
					adapter = new LazyAdapter(SubjectDetails.this, productsList);
					list.setAdapter(adapter);
				}
			});

		}
	}

	class GetDownloadLink extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog1 = new ProgressDialog(SubjectDetails.this);
			pDialog1.setMessage("Loading Details. Please wait...");
			pDialog1.setIndeterminate(false);
			pDialog1.setCancelable(false);
			pDialog1.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("subjectID", subjectID));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(file_urlLink, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			// Checking for SUCCESS TAG
			int success;
			try {
				success = json.getInt("success");

				if (success == 1) {
					filename = json.getString("reference");
					dwnLink = session.moodleRoot + "file.php/" + subjectID
							+ "/" + filename;
					// file_urlDownload = dwnLink;
					Log.e("LINK", dwnLink);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// else {
			// no products found
			// Launch Add New product Activity
			// }
			// } catch (JSONException e) {
			// e.printStackTrace();
			// }
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog1.dismiss();
			new DownloadFileFromURL().execute(dwnLink);
		}
	}

	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog2 = new ProgressDialog(SubjectDetails.this);
			pDialog2.setMessage("Downloading file. Please wait...");
			pDialog2.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					((AlertDialog) dialog).getButton(which).setVisibility(
							View.INVISIBLE);
				}
			});
			pDialog2.setIndeterminate(false);
			pDialog2.setMax(100);
			pDialog2.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog2.setCancelable(false);
			pDialog2.show();
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream("/sdcard/"
						+ filename);

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog2.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
					SubjectDetails.this);
			dlgAlert.setMessage(filename + " Downloaded Successfully");
			dlgAlert.setTitle("Download Complete");
			dlgAlert.setPositiveButton("OK", null);
			dlgAlert.setCancelable(false);
			dlgAlert.create().show();
			dlgAlert.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			removeDialog(progress_bar_type);

			// Displaying downloaded image into image view
			// Reading image path from sdcard
			// String imagePath =
			// Environment.getExternalStorageDirectory().toString() +
			// "/downloadedfile";
			// setting downloaded into image view
			// my_image.setImageDrawable(Drawable.createFromPath(imagePath));
		}
	}

	class LazyAdapter extends BaseAdapter {

		private Activity activity;
		private ArrayList<HashMap<String, String>> data;
		private LayoutInflater inflater = null;

		// public ImageLoader imageLoader;

		public LazyAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
			activity = a;
			data = d;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// imageLoader=new ImageLoader(activity.getApplicationContext());
		}

		public int getCount() {
			return data.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			// if(convertView==null)
			// try{
			HashMap<String, String> song = new HashMap<String, String>();
			song = data.get(position);

			if (song.get("module").equals("14")) {
//				if (convertView == null)
					vi = inflater.inflate(R.layout.listrow_resource, null);

				TextView title = (TextView) vi.findViewById(R.id.id); // title
				TextView artist = (TextView) vi.findViewById(R.id.name); // artist
																			// name
				TextView duration = (TextView) vi.findViewById(R.id.module); // duration
				TextView thumb_image = (TextView) vi
						.findViewById(R.id.instance); // thumb image
				ImageView imageRes = (ImageView) findViewById(R.id.resPic);

				// Setting all values in listview
				title.setText(song.get("id"));
				artist.setText(song.get("name"));
				duration.setText(song.get("module"));
				thumb_image.setText(song.get("instance"));
			}
			if (song.get("module").equals("1")) {
//				if (convertView == null)
					vi = inflater.inflate(R.layout.listrow_assignment, null);

				TextView title = (TextView) vi.findViewById(R.id.id); // title
				TextView artist = (TextView) vi.findViewById(R.id.name); // artist
																			// name
				TextView duration = (TextView) vi.findViewById(R.id.module); // duration
				TextView thumb_image = (TextView) vi
						.findViewById(R.id.instance); // thumb image
				ImageView imageRes = (ImageView) findViewById(R.id.assPic);

				// Setting all values in listview
				title.setText(song.get("id"));
				artist.setText(song.get("name"));
				duration.setText(song.get("module"));
				thumb_image.setText(song.get("instance"));

			}
			if (song.get("module").equals("0")) {
				//if (convertView == null)
					vi = inflater.inflate(R.layout.seperater, null);

				vi.setClickable(true);

				TextView title = (TextView) vi.findViewById(R.id.id); // title
				TextView artist = (TextView) vi.findViewById(R.id.separator); //
				// artist
				// name
				TextView duration = (TextView) vi.findViewById(R.id.module);
				// // duration
				TextView thumb_image = (TextView) vi
						.findViewById(R.id.instance); // thumb image

				// Setting all values in listview
				title.setText(song.get("id"));
				artist.setText(song.get("name"));
				duration.setText(song.get("module"));
				thumb_image.setText(song.get("instance"));

			}

			// } catch (Exception e) {
			// Log.e("List: ", e.getMessage());
			// }
			return vi;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.icon_menu, menu);

		if (session.isLoggedIn() == true) {
			menu.add(0, 1, Menu.NONE, name);
			menu.add(0, 4, Menu.NONE, "Notices");
			menu.add(0, 5, Menu.NONE, "Forums");
			menu.add(0, 7, Menu.NONE, "Refresh");
			if (role.equals("Teacher")) {
				menu.add(0, 6, Menu.NONE, "Upload Materials");
			}
			menu.add(0, 2, Menu.NONE, "Logout");

			getMenuInflater().inflate(R.menu.logged_in, menu);
		} else {
			menu.add(0, 3, Menu.NONE, "Login");
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(),
					MainTabs.class);
			startActivity(profile);
			return true;
		}

		if (id == 1) {
			Intent profile = new Intent(getApplicationContext(), Profile.class);
			startActivity(profile);
			return true;
		}

		if (id == 2) {
			session.logoutUser();
			return true;
		}

		if (id == 3) {
			Intent loginAct = new Intent(getApplicationContext(),
					LoginActivity.class);
			startActivity(loginAct);
			return true;
		}


		if (id == R.id.action_mail) {

		}

		if (id == 4) {
			Intent noticesView = new Intent(getApplicationContext(),
					CourseNotices.class);
			noticesView.putExtra("id", subjectID);
			noticesView.putExtra("name", itemName);
			startActivityForResult(noticesView, 100);
		}

		if (id == 5) {
			Intent forum = new Intent(getApplicationContext(), Forums.class);
			forum.putExtra("id", subjectID);
			forum.putExtra("name", itemName);
			
			startActivityForResult(forum, 100);
		}
		
		if(id == 7) {
			finish();
			startActivity(getIntent());
		}
		
		if (id == 6) {
			Intent uploadMaterials = new Intent(getApplicationContext(),
					UploadMaterials.class);
			uploadMaterials.putExtra("id", subjectID);
			startActivityForResult(uploadMaterials, 100);
		}

		if (id == R.id.ic_action_event) {
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null,
				//	CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(),CalendarActivity.class);
			startActivity(calendar);
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onRestart() {
		super.onRestart();
		finish();
		startActivity(getIntent());
	}
}
