package com.moodleapp;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import ar.com.daidalos.afiledialog.FileChooserDialog;

public class UploadAssignments extends Activity {
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

		// Session Manager Class
	SessionManager session;

		// to get user id and name
	String uid, name, role;

		// to get the menu item to store user's name
	MenuItem itemUser;
	String uploadFilePath;
	String uploadFileName;
	String uploadassignment = session.strUrl + "UploadFiles.php";
	String currentAssignment = session.strUrl + "CurrentAssignmentFile.php";
	String uploadAssignmentTableUpdate = session.strUrl + "UploadAssignmentTableUpdates.php"; 
	private ProgressDialog pDialog;
	int serverResponseCode = 0;
	String subjectID;
	String itemName;
	String itemModule;
	String itemInstance;
	String uploadParams;
	JSON_Parse jParser = new JSON_Parse();
	String fileName;
	int success;
	TextView tv;
	String update;
	String assignmentType;
	String resubmit;
	Button btnBrowseFile;
	Button btnUpload;
	int assignment;
	private ActionBar actionBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_assignment_layout);
			
		// object reference to session manager class
		session = new SessionManager(getApplicationContext());

				// check login
				// session.checkLogin();

				// get user data from session
		HashMap<String, String> user = session.getUserDetails();

				// store user id
		uid = user.get(SessionManager.KEY_USERID);

				// store username
		name = user.get(SessionManager.KEY_NAME);

		role = user.get(SessionManager.KEY_ROLE);
		
		Intent i = getIntent();

		// getting product id (pid) from intent
		subjectID = i.getStringExtra("id");
		itemName = i.getStringExtra("name");
		itemModule = i.getStringExtra("module");
		itemInstance = i.getStringExtra("instance");
		
		Bundle extras = i.getExtras();
		if(extras.containsKey("id"))
        {
		subjectID = extras.getString("id");
        }
		if(extras.containsKey("name"))
        {
		itemName = extras.getString("name");
        }
		if(extras.containsKey("module"))
        {
		itemModule = extras.getString("module");
        }
		if(extras.containsKey("instance"))
		{
		itemInstance = extras.getString("instance");
		}
		
		actionBar = getActionBar();
		
		uploadParams = subjectID + "/moddata/assignment/" + itemInstance + "/" + uid;
		
		new GetCurrentFile().execute();
		
		TextView lblAssignmentHeader = (TextView) findViewById(R.id.lblCourses);
		lblAssignmentHeader.setText(itemName);
		
		tv = (TextView) findViewById(R.id.UploadedlblFileName);

		btnBrowseFile = (Button) findViewById(R.id.btnBrowseFile);
		btnBrowseFile.setOnClickListener(btnDialogSimpleOpen);

		btnUpload = (Button) findViewById(R.id.btnUploadFile);
		btnUpload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {		
				new UploadAssignment().execute();
			}
		});
	}

	private OnClickListener btnDialogSimpleOpen = new OnClickListener() {
		public void onClick(View v) {
			// Create the dialog.
			FileChooserDialog dialog = new FileChooserDialog(
					UploadAssignments.this);

			// Assign listener for the select event.
			dialog.addListener(UploadAssignments.this.onFileSelectedListener);

			// Show the dialog.
			dialog.show();
		}
	};

	private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
		public void onFileSelected(Dialog source, File file) {
			source.hide();
			TextView txtFileName = (TextView) findViewById(R.id.lblFilePath);
			txtFileName.setText(file.getName());
			uploadFileName = file.getName();
			uploadFilePath = file.getAbsolutePath();
		}

		public void onFileSelected(Dialog source, File folder, String name) {
			source.hide();
		}
	};
	
	class GetCurrentFile extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(UploadAssignments.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("subjectID", uploadParams));
			params.add(new BasicNameValuePair("assignmentID", itemInstance));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(currentAssignment, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				success = json.getInt("success");
				assignment = json.getInt("assignment");
				fileName = "No File Name";

				if (assignment == 1) {
					// products found
					// Getting Array of Products
					if (success == 1)
						fileName = json.getString("filename");
					assignmentType = json.getString("assignmenttype");
					resubmit = json.getString("resubmit");
				} else {
					// no products found
					// Launch Add New product Activity
				}
				Log.e("DetailsError", fileName + " / " + assignmentType + " / " + resubmit);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			
			if (success == 1)
				tv.setText(fileName);
			else {
				tv.setText("No Files Uploaded");
			}
			
			if (!assignmentType.equals("uploadsingle")) {
				btnBrowseFile.setEnabled(false);
				btnUpload.setEnabled(false);
			}
			
			if (success == 1 && resubmit.equals("0")) {
				btnBrowseFile.setEnabled(false);
				btnUpload.setEnabled(false);
			}
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
				
				}
			});

		}
	}

	class UploadAssignment extends AsyncTask<String, String, String> {

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(uploadFilePath);
		private FileInputStream fileInputStream;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(UploadAssignments.this);
			pDialog.setMessage("Loading Details. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			// open a URL connection to the Servlet
			try {
				fileInputStream = new FileInputStream(sourceFile);
				URL url = new URL(uploadassignment + "?subjectID=" + uploadParams + "&type=1");

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", uploadFilePath);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=uploaded_file;filename="
						+ uploadFilePath + "" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ serverResponseMessage + ": " + serverResponseCode);
				
				if (tv.getText().equals("No Files Uploaded"))
					update = "0";
				else 
					update = "1";
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("update", update));
				params.add(new BasicNameValuePair("assignmentID", itemInstance));
				params.add(new BasicNameValuePair("userID", uid));
				
				JSONObject json = jParser.makeHttpRequest(uploadAssignmentTableUpdate, "GET",
						params);
				
				Log.d("Upload Table Update: ", json.toString());
			} catch (Exception e) {

			}
			return null;
		}

		/**
		 * Updating progress bar
		 * */
		/*
		 * protected void onProgressUpdate(String... progress) { // setting
		 * progress percentage
		 * pDialog.setProgress(Integer.parseInt(progress[0])); }
		 */

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialog.dismiss();
			
			finish();
			startActivity(getIntent());
			// statusUpload = true;
			// new UploadTableUpdate().execute();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(),
					CoursesTab.class);
			startActivity(profile);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}