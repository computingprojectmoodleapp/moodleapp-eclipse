package com.moodleapp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.StringTokenizer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("ValidFragment")
public class AddEventActivity extends Activity
{
	//declare a tag for the activity
	private static final String TAG_EVENT = "addEventActivity";
	
	//declare the properies of this class
	private EditText etTitle;
	private EditText etDescription;
	private Button btnFromDate;
	private Button btnToDate;
	private Button btnFromTime;
	private Button btnToTime;
	private CheckBox cbWithoutDuration;
	
	//to get the date mont year respectively
	private String initialDate;
	private String initialMonth;
	private String initialYear;
	
	//to get the initial time in the button text
	private static int initialHour = -1;
	private static int initialMinute = -1;
	
	//date picker to pick dates
	private DatePickerDialog dateDialog = null;
	
	//time picker to pick time
	private TimePickerDialog timeDialog = null;
	
	//to access the current context
	Context context;
	
	//to access a particular button at a time
	private static View CurrentButtonView = null;
	
	//to access the currently picked date
	public static String strSelectedToDate = "";
	public static String strSelectedFromDate = "";
	
	//to access the currently picked time
	private int strSelected_ToTimeHour = -1;
	private int strSelected_ToTimeMinute = -1;
	private int strSelected_FromTimeHour = -1;
	private int strSelected_FromTimeMinute = -1;
	
	//to get the name of a selected month
	private final String[] months = { "January", "February", "March",
			"April", "May", "June", 
			"July", "August", "September",
			"October", "November", "December" };
	
	//instanse of JSON_Parser class
	JSON_Parse jParser = new JSON_Parse();
	//json array to get the data from db
	JSONArray jArray = null;
	
	//to check if the adding a event is successful
	boolean success;
	
	//to insert the values to db
	public String eventTitle;
	public String descriptioin;
	public String userId;
	public String fromDate;
	public String toDate;
	public String fromTime;
	public String toTime;
	public String msg;
	
	//instance of sessionManager class
	SessionManager session;
	
	//instance of AlertDialogManager class
	AlertDialogManager alert = new AlertDialogManager();
	
	//call when the activity first created
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_event);
		
		//access the controls
		cbWithoutDuration = (CheckBox) findViewById(R.id.cbWithoutDuration);
		etTitle = (EditText) findViewById(R.id.etTitle);
		etDescription = (EditText) findViewById(R.id.etDescription);
		
		//getting userid from sessions
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		userId = user.get(SessionManager.KEY_USERID);
				
		Log.d(TAG_EVENT, "datepicker onCreate");
		dateTimePickers();
		
	}
	
	//set the menu bar
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
    	getMenuInflater().inflate(R.menu.calendar_menu, menu);
		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int id = item.getItemId();
		
    	Log.d("save click", "checking");
    	if(id == R.id.action_save_event)
    	{
    		
    			eventTitle = etTitle.getText().toString();
    			Log.d("save click", "title = " + eventTitle);
    			descriptioin = etDescription.getText().toString();
    			Log.d("save click", "des = " + descriptioin);
    			fromDate = strSelectedFromDate;
    			Log.d("save click", "from date = " + fromDate);
    			fromTime = strSelected_FromTimeHour + ":" + strSelected_FromTimeMinute;
    			Log.d("save click", "fromtime = " + fromTime);
    			
    			if (!eventTitle.equals("")) {
    				
    				if(cbWithoutDuration.isChecked() == true)
	    			{
	    				Log.d("save click", "inside if ischecked = "+ cbWithoutDuration.isChecked());
	    				toDate = "0000-00-00";
	    				Log.d("save click", "to date = " + toDate);
	    				toTime = "00:00";
	    				Log.d("save click", "to time = " + toTime);
	    				new AddEvent().execute();
	    			}
	    			else
	    			{
	    				
	    				String pattern = "dd/MM/yyyy HH:mm";
	    				SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
	    				try {
	    					
							Date ToDate = dateFormat.parse(strSelectedToDate + " " + strSelected_ToTimeHour + ":" + strSelected_ToTimeMinute);
							Date fromDate = dateFormat.parse(strSelectedFromDate + " " + strSelected_FromTimeHour + ":" + strSelected_FromTimeMinute);
							
							long timeDiff = Math.abs(ToDate.getTime() - fromDate.getTime());
							
							if (timeDiff <= 0) {
								Log.d("timeDiff","timeDIFF" + timeDiff);
								Toast.makeText(getApplicationContext(), "Invalid time", Toast.LENGTH_LONG).show();
								
							}
							else {
								
								Log.d("save click", "inside else ischecked = "+ cbWithoutDuration.isChecked());
			    				toDate = strSelectedToDate;
			    				Log.d("save click", "to date = " + toDate);
			    				toTime = strSelected_ToTimeHour + ":" + strSelected_ToTimeMinute;
			    				Log.d("save click", "to time = " + toTime);
			    				new AddEvent().execute();
							}
						} catch (ParseException e) {
							
						}
	    				
	    				
	    				
	    			}
    			}
    			else {
    				Toast.makeText(getApplicationContext(), "Please enter a title for the event..!", Toast.LENGTH_LONG).show();
    			}
    		
    	}
		return super.onOptionsItemSelected(item);
	}
	
    @Override
    public void onRestart() { 
        super.onRestart();
        finish();
		startActivity(getIntent());
    }

    //methods for button click events
	public void dateTimePickers()
	{
		//to get a time picker for start day when click on btnFromDate
		Log.d(TAG_EVENT, "datepicker clicks=from");
		btnFromDate = (Button) findViewById(R.id.btnFromDate);
		btnFromDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try 
				{
					Log.d(TAG_EVENT, "displayDatePicker()-from" + strSelectedFromDate);
					DisplayDatePicker(v, strSelectedFromDate);
				}
				catch (Exception ex) 
				{
					
				}
				
			}});
		Log.d(TAG_EVENT, "setText-fromdate- displayDate()" + strSelectedFromDate);
		//set selected date as the text of the button
		btnFromDate.setText(DisplayDate(strSelectedFromDate));
		
		//to get a time picker for start day when click on btnToDate
		Log.d(TAG_EVENT, "datepicker clicks=to");
		btnToDate = (Button) findViewById(R.id.btnToDate);
		btnToDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try 
				{
					Log.d(TAG_EVENT, "displayDatePicker()-t0" + strSelectedFromDate);
					DisplayDatePicker(v, strSelectedToDate);
				}
				catch (Exception ex) {
					
				}
				
			}});
		Log.d(TAG_EVENT, "setText-toate- displayDate()" + strSelectedFromDate);
		//set selected date as the text of the button
		btnToDate.setText(DisplayDate(strSelectedToDate));
		
		//to get a time picker for start day when click on btnFromTime
		Log.d(TAG_EVENT,"dateTimePicker-fromTime Click");
		btnFromTime = (Button) findViewById(R.id.btnFromTime);
		btnFromTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d(TAG_EVENT, "ONCLICK-fromtime");
				//showTimePickerDialog(v);
				DisplayTimePicker(v, initialHour, initialMinute);
				
			}
		});
		//set selected time as the text of the button
		btnFromTime.setText(DisplayTime(strSelected_FromTimeHour,strSelected_FromTimeMinute));
		strSelected_FromTimeHour = initialHour;
		strSelected_FromTimeMinute = initialMinute;
				
		//to get a time picker for start day when click on btnFromTime
		Log.d(TAG_EVENT,"dateTimePicker-toTime Click");
		btnToTime = (Button) findViewById(R.id.btnToTime);
		btnToTime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d(TAG_EVENT, "ONCLICK-totime");
				DisplayTimePicker(v,initialHour, initialMinute);
			}
		});
		//set selected time as the text of the button
		btnToTime.setText(DisplayTime(strSelected_ToTimeHour,strSelected_ToTimeMinute));
		strSelected_ToTimeHour = initialHour;
		strSelected_ToTimeMinute = initialMinute;
		
		//check box checked changing
		cbWithoutDuration.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				//check if the check box is checked
				if(isChecked)
				{
					//disable the button for end date
					btnToDate.setEnabled(false);
					btnToTime.setEnabled(false);
				}
				else
				{
					//enable button for end date
					btnToDate.setEnabled(true);
					btnToTime.setEnabled(true);
				}
			}
		});
		
	}

	//display the seleceted date on the text of button
	private String DisplayDate(String strDate) {
		
		try {
			Log.d(TAG_EVENT,"inside Display date - passing strSelectedFromDate");
			StringTokenizer st = new StringTokenizer(strDate, "/");
			initialDate = st.nextToken();
			initialMonth = st.nextToken();
			initialYear = st.nextToken();
			Log.d(TAG_EVENT,"inside Display date -string token-> "+ st + "-" + initialDate + "-" + initialMonth + "-" +initialYear);
			
			int intIndex = Integer.parseInt(initialMonth);
			Log.d(TAG_EVENT,"inside display date-returns" + initialDate + " " + months[intIndex] + " " + initialYear);
			return initialDate + " " + months[intIndex] + " " + initialYear;
		}
		catch (Exception ex) {
			return "";
		}
	}
	
	//create a date picker
	private void DisplayDatePicker(View ButtonView, String preExistingDate) {
		
		try {
			
			CurrentButtonView = ButtonView;
			Calendar dtTxt = null;
		
			
			if (preExistingDate != null && !preExistingDate.equals(""))
			{
				Log.d(TAG_EVENT, "if preexistinng null");
				StringTokenizer st = new StringTokenizer(preExistingDate,"/");
				initialDate = st.nextToken();
				initialMonth = st.nextToken();
				initialYear = st.nextToken();
				Log.d(TAG_EVENT, "string token-> "+ st + "-" + initialDate + "-" + initialMonth + "-" +initialYear);
				
				if(dateDialog == null)
				{
					Log.d(TAG_EVENT, "if dateDialog null");
					Log.d(TAG_EVENT, "update date");
					Log.d(TAG_EVENT, " dd 0" +dateDialog);
					dateDialog = new DatePickerDialog(ButtonView.getContext(), new PickDate(),Integer.parseInt(initialYear),
							Integer.parseInt(initialMonth),
							Integer.parseInt(initialDate));
					Log.d(TAG_EVENT, " dd 1" +dateDialog);
				}
					
				dateDialog.updateDate(Integer.parseInt(initialYear),
						Integer.parseInt(initialMonth),
						Integer.parseInt(initialDate));
				Log.d(TAG_EVENT, " dd 2" +dateDialog);
	                  
			} 
			else 
			{
				Log.d(TAG_EVENT, "else preexistinng null");
				dtTxt = Calendar.getInstance();
				if(dateDialog == null)
				{
					Log.d(TAG_EVENT, "if dateDialog null");
					Log.d(TAG_EVENT, "update date");
					Log.d(TAG_EVENT, " dd 0" +dateDialog);
					dateDialog = new DatePickerDialog(ButtonView.getContext(),new PickDate(),dtTxt.getTime().getYear(),dtTxt.getTime().getMonth(),
							dtTxt.getTime().getDay());
					Log.d(TAG_EVENT, " dd 1" +dateDialog);
				}
				Log.d(TAG_EVENT, "update date");
				dateDialog.updateDate(dtTxt.getTime().getYear(),dtTxt.getTime().getMonth(), dtTxt.getTime().getDay());
				Log.d(TAG_EVENT, " dd 0" +dateDialog);
			}
			Log.d(TAG_EVENT, "show dialog");   
			dateDialog.show();
		}
		catch (Exception ex) {
			
		}
	}
	
	//when picked a date
	private class PickDate implements DatePickerDialog.OnDateSetListener {

	    @Override
	    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	        view.updateDate(year, monthOfYear, dayOfMonth);
	        Log.d(TAG_EVENT, "onDateSET INSIDE");
	        
	        //get the currently selected date from the picker
	        String strDate = dayOfMonth + "/" + monthOfYear + "/" + year;
	        Log.d(TAG_EVENT, strDate);
	        
	        if (CurrentButtonView != null) {
	        	((Button)CurrentButtonView).setText(DisplayDate(strDate));
	        	
	        	//if the selected button is btnToDate
	        	if (CurrentButtonView.getId() == R.id.btnToDate)
	        	{
	        		//change its text to selected date
	        		Log.d(TAG_EVENT, "inside if btnToDate");
	        		strSelectedToDate = strDate;
	        	}
	        	//if the selected button is btnToDate
	        	else if (CurrentButtonView.getId() == R.id.btnFromTime)
	        	{
	        		//change its text to selected date
	        		Log.d(TAG_EVENT, "inside if btnToDate");
	        		strSelectedFromDate = strDate;
	        	}
	        }
	        
	        Log.d(TAG_EVENT, "datedialog hide");
	        dateDialog.hide();
	        Log.d(TAG_EVENT, "datedialog=null");
	        dateDialog = null;
	    }
	    
	}
	
	//have a time picker to pick a time
	private void DisplayTimePicker(View ButtonView, int initialHour, int initialMinute) {
			
			try {
				
				CurrentButtonView = ButtonView;
				Calendar dtTxt = null;
				
				if (initialHour != -1 && initialMinute != -1) {
					
					if (timeDialog == null)
						timeDialog = new TimePickerDialog(ButtonView.getContext(), new PickTime(), initialHour, initialMinute, true);
					
					timeDialog.updateTime(initialHour, initialMinute);
				} 
				else {
		
					dtTxt = Calendar.getInstance();
					
					initialHour = dtTxt.getTime().getHours();
					initialMinute = dtTxt.getTime().getMinutes();
					strSelected_ToTimeHour = initialHour;
					strSelected_ToTimeMinute = initialMinute;
					
					if (timeDialog == null)
						timeDialog = new TimePickerDialog(ButtonView.getContext(), new PickTime(), initialHour, initialMinute, true);
			
					timeDialog.updateTime(dtTxt.getTime().getHours(), dtTxt.getTime().getMinutes());
				}
				
				timeDialog.show();
			}
			catch (Exception ex) {
				
			}
		}
	
	public class PickTime implements TimePickerDialog.OnTimeSetListener 
	{
	
		public void onTimeSet(TimePicker view, int hourOfDay, int minute)
		{
			// Do something with the time chosen by the user"
			try
			{				
						        
		        if (CurrentButtonView != null) {
		        	
		        	((Button)CurrentButtonView).setText(DisplayTime(hourOfDay, minute));
		        	
		        	//if the selected button is btnTo
		        	if (CurrentButtonView.getId() == R.id.btnToTime) {
		        
		        		//change the text of the button to selected time
		        		strSelected_ToTimeHour = hourOfDay;
		        		strSelected_ToTimeMinute = minute;
		        		
		        	}
		        	else if (CurrentButtonView.getId() == R.id.btnFromTime){
		     
		        		//change the text of the button to selected time
		        		strSelected_FromTimeHour = hourOfDay;
		        		strSelected_FromTimeMinute = minute;
		        	}
		        	
		        }
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(), "Exception : "+ ex.getMessage(), Toast.LENGTH_LONG).show();
			}
		}
	}
	
	private String DisplayTime(int intHour, int intMinute) 
	{
		
		try
		{
			
			if (intHour != -1 && intMinute != -1) {
				
				initialHour = intHour;
				initialMinute = intMinute;
			}
			else {
				
				Calendar dtTxt = null;
				dtTxt = Calendar.getInstance();
				//get the current time
				initialHour = dtTxt.getTime().getHours();
				initialMinute = dtTxt.getTime().getMinutes();
			}
			
			return initialHour + " : " + initialMinute;
		}
		catch (Exception ex) 
		{
			return "";
		}
	}

	class AddEvent extends AsyncTask<String, String, String>{
			
			@Override
			protected void onPreExecute(){
				super.onPreExecute();
			}
			
			protected String doInBackground(String...args){
				//building parameters
				
				fromDate = fromDate.replace('/', '-');
				toDate = toDate.replace('/', '-');
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("eventTitle", eventTitle));
				params.add(new BasicNameValuePair("eventDes", descriptioin));
				params.add(new BasicNameValuePair("userid", userId));
				params.add(new BasicNameValuePair("fromDate", fromDate));
				params.add(new BasicNameValuePair("fromTime", fromTime));
				params.add(new BasicNameValuePair("toDate", toDate));
				params.add(new BasicNameValuePair("toTime", toTime));
				
				Log.d("doInbackground addEvent", "working");
				
				//getting JSON string URL
				JSONObject json = jParser.makeHttpRequest(session.strUrl + "addEvent.php", "GET", params);
				
				Log.d("getting json object addEvent", "working");
				
				//check log cat for JSON response
				Log.d("status: addEvent", json.toString());
				
				try
				{
					success = json.getBoolean("success");
					Log.d("getting success addEvent", "working");
					
					msg = json.getString("message");
					
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
				
				return null;
			}
			
			protected void onPostExecute(String filepath){
			
				Log.d("onPostExecute", "checking if etTitle null");
				
						Log.d("onPostExecute", "inside if date comparing");
						if (success)
						{
							Log.d("onPostExecute", "inside if SUCCESS");
							Log.d("onPostExecute", "SUCCESS :=>" +success);
							alert.showAlertDialog(AddEventActivity.this, "Done!", msg, false);
							//sendNotification(R.drawable.ic_launcher, "Event", "New Event", "New Event has been added", EventPreview.class);
							etTitle.setText("");
							etDescription.setText("");
							//Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
							
						}
						else
						{
							Log.d("onPostExecute", "inside ELSE SUCCESS");
							alert.showAlertDialog(AddEventActivity.this, "Error!", msg, false);
							etTitle.setText("");
							etDescription.setText("");
						}
				
			}
		}
}