package com.moodleapp;
import android.app.ListActivity;
import android.os.Bundle;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class HomeTab extends ListActivity {
	
	String data = "";
	TableLayout tl;
	TableRow tr;
	TextView label;
	
	private ProgressDialog dialog;
	JSON_Parse getDB = new JSON_Parse();
	ArrayList<HashMap<String, String>> allNotices;
	JSONArray jArray = null;
	
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_tab_layout);
		
		new ImageDownloader((ImageView) findViewById(R.id.imgVw1)).execute("http://courseweb.sliit.lk/pluginfile.php/2/course/section/2/SQAP%20Program.png");
		new ImageDownloader((ImageView) findViewById(R.id.imgVw2)).execute("http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/PDP.png");
		new ImageDownloader((ImageView) findViewById(R.id.imgVw3)).execute("http://courseweb.sliit.lk/pluginfile.php/10443/block_html/content/Contact.png");
		
		allNotices = new ArrayList<HashMap<String, String>>();
		
		//loading notices in background thread
		new LoadAllNotices().execute();
		
		ImageView imgBtn = (ImageView) findViewById(R.id.imgVw1);
		imgBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.sliit.lk/pd_programmes/software-quality-assurance-professional-program-sqa-program/"));
				intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				
				Context con = getApplicationContext();
				CharSequence text = "Redirecting to http://www.sliit.lk...";
				Toast msg = Toast.makeText(con, text, Toast.LENGTH_SHORT);
				msg.show();
				
			}
		});
		
		//enabling the overflow icon regardless of the device
		try{
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if(menuKeyField != null){
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config,false);
			}
		} catch(Exception ex){
			//Ignore
		}
		
	}
	
	//Loading images through a give URL
	class ImageDownloader extends AsyncTask<String, Void, Bitmap>{
		ImageView bmImage;
		
		public ImageDownloader(ImageView image){
			this.bmImage = image;
		}
		
		@Override
		protected Bitmap doInBackground(String... params) {
			String url = params[0];
			Bitmap mIcon = null;
			
			try{
				InputStream is = new java.net.URL(url).openStream();
				mIcon = BitmapFactory.decodeStream(is);
			} 
			catch(Exception e){
				Log.e("Error!",e.getMessage());
			}	
			return mIcon;
		}
		protected void onPostExecute(Bitmap result){
			bmImage.setImageBitmap(result);
		}
	}
	
	
	class LoadAllNotices extends AsyncTask<String, String, String>{
		
		//show process dialog
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			dialog = new ProgressDialog(HomeTab.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		//getting notices from URL
		protected String doInBackground(String...args){
			//Building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			String nope = "0";
			params.add(new BasicNameValuePair("nill", nope));
			
			//getting JSON string from URL
			JSONObject json = getDB.makeHttpRequest(session.strUrl + "newnotices.php", "GET", params);
			
			//check log for JSON response
			Log.d("All notices", json.toString());
			
			try{
				jArray = json.getJSONArray("notices");
				
				//traversing through the JSONArray
				for (int i=0; i<jArray.length(); i++){
					JSONObject note = jArray.getJSONObject(i);
					
					//storing each item value in variables
					String subject = note.getString("subject");
					String content = note.getString("content");
					
					//Creating a HashMap
					HashMap<String, String> map = new HashMap<String, String>();
					
					//adding each child node to HashMap
					map.put("subject", subject);
					map.put("content", content);
					
					allNotices.add(map);
				}
			} catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String filepath){
			dialog.dismiss();
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					//Updating parsed JSONData into a ListView
					ListAdapter adapter = new SimpleAdapter(HomeTab.this, allNotices, R.layout.home_notices, new String[] {"subject","content"}, new int[] {R.id.notice_subject, R.id.notice_content});
					setListAdapter(adapter);
					
				}
			});
			
		}
	}

}
