package com.moodleapp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

//import android.security.
public class LoginActivity extends Activity {

	// edittexts for username and password
	EditText txtUsername, txtPassword;

	// login button
	Button btnLogin;

	// Alert Dialog Manager Class
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;

	// Progress Dialog
	private ProgressDialog pDialog;

	// json class
	JSON_Parse jParser = new JSON_Parse();

	// string variable to store usrname password user id name and roleName
	String username, password, userid, name, roleName;

	// to check if there rows in the database
	boolean var;

	// url for database
	private String loginUrl = session.strUrl + "Login.php";
	private String notifications = session.strUrl + "NotificationThread.php";

	// json class object
	JSONObject json;
	
	public int count = 3;
	
	String lastlogin;
	String currentlogin;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login); 
        
        // object reference to Session Manager class
        session = new SessionManager(getApplicationContext());                
        
        // Username, Password input text
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword); 
        
        // Login button
        btnLogin = (Button) findViewById(R.id.btnLogin);
        
        // Login button click event
        btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				// Get username and password from EditText
				username = txtUsername.getText().toString();
				password = txtPassword.getText().toString();
				
				//clear the text boxes
				txtUsername.setText("");
				txtPassword.setText("");
				
				//call the method for thread
				new LoadUserDetails().execute();
				
			}
		});
    } 
    
    class LoadUserDetails extends AsyncTask<String, String, String> {
	   	 
        //Before starting background thread Show Progress Dialog
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        //get all the details needed to compare with db
        protected String doInBackground(String... args) {
        	
            // Building Parameters
        	List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("username", username));
			params.add(new BasicNameValuePair("password",password));
			
            // getting JSON string from URL
            json = jParser.makeHttpRequest(loginUrl, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("User Details: ", json.toString());
 
            try 
            {
            	//retrieve the data from database
				var = json.getBoolean("logged");
				name = json.getString("name");
				userid = json.getString("uid");
				roleName = json.getString("role");
				lastlogin = json.getString("lastlogin");
				currentlogin = json.getString("currentlogin");
            } 
            catch (JSONException e) 
            {
                e.printStackTrace();
            }
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
        	
			// dismiss the dialog after getting all products
			pDialog.dismiss();	
			
			//if the input data is matching with the data in the database
			if(username.length() > 0 && password.length() > 0)
			{
				if(var)
				{
					// Creating user login session - storing userid and name
					session.createLoginSession(userid, name, roleName);
					
					//check if the Login is only allowed for lectures and students
					if(roleName.equals("Teacher") || roleName.equals("Student"))
					{
						// Staring MainActivity
						//Intent i = new Intent(getApplicationContext(), MainTabs.class);
						//startActivity(i);
						finish();
						new NotificationThread().execute();
					}	
					else
					{
						alert.showAlertDialog(LoginActivity.this, "Authentication Error", "Login is not allowed", false);
					}
				}
				else
				{
					alert.showAlertDialog(LoginActivity.this, "Login failed..", "Username/Password is incorrect", false);
					count--;
					
					if(count <= 0){
						alert.showAlertDialog(LoginActivity.this, "Warning!", "You seem to have forgotten your password. Please contact your administrator.", false);
					}
				}	
			}
			else
			{
				alert.showAlertDialog(LoginActivity.this, "Login failed..", "Username and Password are required", false);
			}
			
		}
	}

	class NotificationThread extends AsyncTask<String, String, String> {

		// Before starting background thread Show Progress Dialog
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
		}

		// get all the details needed to compare with db
		protected String doInBackground(String... args) {

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userid", userid));
			params.add(new BasicNameValuePair("lastlogin", lastlogin));
			params.add(new BasicNameValuePair("currentlogin", currentlogin));

			// getting JSON string from URL
			json = jParser.makeHttpRequest(notifications, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("Notification Details: ", json.toString());

			try {
				int notifId;
				notifId = 0;
				int suc_meg = json.getInt("success_message");
				if (suc_meg == 1) {
					JSONArray messages = json.getJSONArray("message");
					for (int i = 0; i < messages.length(); i++) {
						JSONObject c = messages.getJSONObject(i);

						// retrieve the data from database
						String name = c.getString("name");
						String message = c.getString("message");
						String timecreated = c.getString("timecreated");
						String useridfrom = c.getString("useridfrom");

						//sendNotification(notifId, R.drawable.ic_launcher, "New Message",
								//"New Message from " + name + " at "
										//+ timecreated, message, Messagers.class);
						try {
							// unique id for a notifaction
							//int notifId = 0;

							// object of notification manager class
							NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

							// cancel all the previous notifications
							//nm.cancelAll();

							// create an explicit intent
							Intent intent = new Intent(getApplicationContext(), Message.class);
							
							intent.putExtra("contactid", useridfrom);
							intent.putExtra("contactblocked", "0");

							// redirecting to an activity
							PendingIntent pending = PendingIntent.getActivity(
									getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

							// set notification properties
							NotificationCompat.Builder ncBuilder = new Builder(
									getApplicationContext())
									.setSmallIcon(R.drawable.ic_launcher)
									.setContentTitle("New Message from " + name + " at " + timecreated)
									.setTicker("New Message")
									.setContentText(message)
									.setAutoCancel(true)
									.setSound(
											RingtoneManager
													.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

							// Puts the PendingIntent into the notification builder
							ncBuilder.setContentIntent(pending);

							// issue the notification
							nm.notify(notifId, ncBuilder.build());
							
							notifId++;

						} catch (Exception ex) {
							// Toast t= Toast.makeText(MainActivity.this, ex.getMessage(),
							// 5000);
						}

					}
				}

				int suc_cou_mat_mr = json.getInt("success_course_materials_mr");
				int suc_cou_mat_as = json.getInt("success_course_materials_as");
				if (suc_cou_mat_mr == 1 || suc_cou_mat_as == 1) {
					JSONArray course_materials = json.getJSONArray("course_materials");
					for (int i = 0; i < course_materials.length(); i++) {
						JSONObject c = course_materials.getJSONObject(i);

						// retrieve the data from database
						String id = c.getString("id");
						String coursename = c.getString("fullname");
						String name = c.getString("name");
						String added = c.getString("added");
						String module = c.getString("module");
						String instance = c.getString("instance");

						//sendNotification(i, R.drawable.ic_launcher, "New Course Material",
						//		"to " + coursename + "at " + added, name, SubjectDetails.class);
						if (module.equals("1"))
						{
							

						try {
							// unique id for a notifaction
							//int notifId = 0;

							// object of notification manager class
							NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

							// cancel all the previous notifications
							//nm.cancelAll();

							// create an explicit intent
							Intent intent = new Intent(getApplicationContext(), UploadAssignments.class);
							
							intent.putExtra("id", id);
							intent.putExtra("name", coursename);
							intent.putExtra("module", module);
							intent.putExtra("instance", instance);
							
							//intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
							
							// redirecting to an activity
							PendingIntent pending = PendingIntent.getActivity(
									getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
							
							//pending.FLAG_CANCEL_CURRENT;

							// set notification properties
							NotificationCompat.Builder ncBuilder = new Builder(
									getApplicationContext())
									.setSmallIcon(R.drawable.ic_launcher)
									.setContentTitle("Assignment on " + coursename + " at" + added)
									.setTicker("New Assignment")
									.setContentText(name)
									.setAutoCancel(true)
									.setSound(
											RingtoneManager
													.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

							// Puts the PendingIntent into the notification builder
							ncBuilder.setContentIntent(pending);

							// issue the notification
							nm.notify(notifId, ncBuilder.build());
							
							notifId++;

						} catch (Exception ex) {
							// Toast t= Toast.makeText(MainActivity.this, ex.getMessage(),
							// 5000);
						}
						}
						
						if (module.equals("14"))
						{
							try {
								// unique id for a notifaction
								//int notifId = 0;

								// object of notification manager class
								NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

								// cancel all the previous notifications
								//nm.cancelAll();

								// create an explicit intent
								Intent intent = new Intent(getApplicationContext(), SubjectDetails.class);
								
								intent.putExtra("id", id);
								intent.putExtra("name", coursename);

								// redirecting to an activity
								PendingIntent pending = PendingIntent.getActivity(
										getApplicationContext(), 0, intent, 0);

								// set notification properties
								NotificationCompat.Builder ncBuilder = new Builder(
										getApplicationContext())
										.setSmallIcon(R.drawable.ic_launcher)
										.setContentTitle("New Course Materials on " + coursename + "at " + added)
										.setTicker("New Course Material")
										.setContentText(name)
										.setAutoCancel(true)
										.setSound(
												RingtoneManager
														.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

								// Puts the PendingIntent into the notification builder
								ncBuilder.setContentIntent(pending);

								// issue the notification
								nm.notify(notifId, ncBuilder.build());
								
								notifId++;

							} catch (Exception ex) {
								// Toast t= Toast.makeText(MainActivity.this, ex.getMessage(),
								// 5000);
							}
						}

					}
				}
				
				JSONArray jNotices = null;
				JSONArray jForums = null;
				String courseid;
				String coursename;
				String message;
				String courseidf;
				String course;
				String discussionId;
				String forumtopic;
				
				int result_notice = json.getInt("notice_success");
				int result_forum = json.getInt("forum_success");
				
				if(result_notice == 1){
					jNotices = json.getJSONArray("notices");
					for(int i=0; i<jNotices.length(); i++){
						JSONObject jj = jNotices.getJSONObject(i);
						
						//retrieve data
						courseid = jj.getString("course");
						coursename = jj.getString("coursename");
						message = jj.getString("message");
						
						//sendNotification(R.drawable.ic_action_new_label, "New Notices!", message, "", SubjectDetails.class);
						try
						{
							//object of notification manager class
							NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
							
							//create an explicit intent
							//to redirect to the MainActivity when the notification is clicked
							Intent intent = new Intent(getApplicationContext(), CourseNotices.class);
							intent.putExtra("id", courseid);
							intent.putExtra("name", coursename);
							intent.setData((Uri.parse("foobar://"+ SystemClock.elapsedRealtime())));
							
							//redirecting to an activity
							PendingIntent pending = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
							
							//set notification properties
							NotificationCompat.Builder ncBuilder = new Builder(getApplicationContext())
							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle("New Notices!")
							.setTicker(message)
							.setContentText(message)
							.setAutoCancel(true)
							.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
							
							//Puts the PendingIntent into the notification builder
							ncBuilder.setContentIntent(pending);
							
							//issue the notification
							nm.notify(notifId, ncBuilder.build());
							notifId++;
							
						}
						catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
					
				}
				
				if(result_forum == 1){
					jForums = json.getJSONArray("forums");
					for(int i=0; i<jForums.length(); i++){
						JSONObject jj = jForums.getJSONObject(i);
						
						//retrieve data
						courseidf = jj.getString("course");
						course = jj.getString("coursename");
						discussionId = jj.getString("discussion");
						forumtopic = jj.getString("topic");
						message = jj.getString("message");
						
						try
						{
							//object of notification manager class
							NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
							
							//create an explicit intent
							//to redirect to the MainActivity when the notification is clicked
							Intent intent = new Intent(getApplicationContext(), ForumPosts.class);
							intent.putExtra("courseid", courseidf);
							intent.putExtra("id", discussionId);
							intent.putExtra("forumtopic", forumtopic);
							intent.setData((Uri.parse("foobar://" + SystemClock.elapsedRealtime())));
							
							
							//redirecting to an activity
							PendingIntent pending = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
							
							//set notification properties
							NotificationCompat.Builder ncBuilder = new Builder(getApplicationContext())
							.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle("New Forum Posts!")
							.setTicker(message)
							.setContentText(message)
							.setAutoCancel(true)
							.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
							
							//Puts the PendingIntent into the notification builder
							ncBuilder.setContentIntent(pending);
							
							//issue the notification
							nm.notify(notifId, ncBuilder.build());
							notifId++;
							
							
						}
						catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {

			// dismiss the dialog after getting all products
			pDialog.dismiss();
		}
	}

	Handler handler = new Handler();

	final Runnable r = new Runnable() {
		public void run() {
			new NotificationThread().execute();
			// handler.postDelayed(this, 10000);
		}
	};

	Thread notificationsThread = new Thread() {
		@Override
		public void run() {
			try {
				while (true) {
					sleep(15000);
					handler.post(r);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	public void sendNotification(int notifId, int statNotifyMore, String tickerText,
			String contentTitle, String contentDetail, Class<?> cls) {

		try {
			// unique id for a notifaction
			//int notifId = 0;

			// object of notification manager class
			NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			// cancel all the previous notifications
			nm.cancelAll();

			// create an explicit intent
			Intent intent = new Intent(getApplicationContext(), cls);

			// redirecting to an activity
			PendingIntent pending = PendingIntent.getActivity(
					getApplicationContext(), 0, intent, 0);

			// set notification properties
			NotificationCompat.Builder ncBuilder = new Builder(
					getApplicationContext())
					.setSmallIcon(statNotifyMore)
					.setContentTitle(contentTitle)
					.setTicker(tickerText)
					.setContentText(contentDetail)
					.setAutoCancel(true)
					.setSound(
							RingtoneManager
									.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

			// Puts the PendingIntent into the notification builder
			ncBuilder.setContentIntent(pending);

			// issue the notification
			nm.notify(notifId, ncBuilder.build());

		} catch (Exception ex) {
			// Toast t= Toast.makeText(MainActivity.this, ex.getMessage(),
			// 5000);
		}

	}
}
