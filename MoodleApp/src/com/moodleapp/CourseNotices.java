package com.moodleapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CourseNotices extends ListActivity {

	String subjectID;
	String coursename;
	int result;
	String msg;
	String noticeid;
	String content;
	String subject;
	String userid;

	private ProgressDialog dialog;
	JSON_Parse jParser = new JSON_Parse();
	ArrayList<HashMap<String, Object>> noticeList;
	JSONArray jArray = null;

	SessionManager session;
	String role;

	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notices_layout);

		Intent intent = getIntent();

		//getting subject id from Intent
		subjectID = intent.getStringExtra("id");
		coursename = intent.getStringExtra("name");

		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();

		//get user's id and role
		userid = user.get(SessionManager.KEY_USERID);
		role = user.get(SessionManager.KEY_ROLE);

		noticeList = new ArrayList<HashMap<String, Object>>();

		//displaying course name
		TextView lblHeader = (TextView) findViewById(R.id.lblcoursename);
		lblHeader.setText(coursename);

		//loading all notices in background thread
		new LoadCourseNotices().execute();
		Log.d("loadcoursenotices", "method works");

		//long-click menu
		ListView list = getListView();
		registerForContextMenu(list);


		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
			                               int position, long id) {
				// TODO Auto-generated method stub
				noticeid = ((TextView) view.findViewById(R.id.noticeid)).getText().toString();
				content = ((TextView) view.findViewById(R.id.content)).getText().toString();
				subject = ((TextView) view.findViewById(R.id.subject)).getText().toString();
				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.notices_menu, menu);

		if (!role.equals("Teacher")) {
			menu.setGroupVisible(Menu.NONE, false);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_new) {
			Intent addView = new Intent(getApplicationContext(), AddCourseNotice.class);
			addView.putExtra("id", subjectID);
			addView.putExtra("name", coursename);
			startActivityForResult(addView, 100);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (role.equals("Teacher")) {
			menu.setHeaderTitle("Notices");
			menu.add(2, 2, 1, "Edit");
			menu.add(2, 1, 1, "Delete");
		}
	}

	//testing the context menu with 
	@Override
	public boolean onContextItemSelected(final MenuItem item) {

		if (item.getItemId() == 1) {
			new AlertDialog.Builder(this).setTitle("Confirm Delete")
				.setMessage("Are you sure you want to delete this notice?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int i) {
						AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
						noticeid = ((TextView) info.targetView.findViewById(R.id.noticeid)).getText().toString();
						new DeleteNotice().execute();
					}
				})
				.setNeutralButton("Cancel", null)
				.create()
				.show();
		}else if(item.getItemId()==2){
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			View targetView = info.targetView;
			ViewHolder viewHolder = (ViewHolder) targetView.getTag();
			noticeid = ((TextView) targetView.findViewById(R.id.noticeid)).getText().toString();
			Intent editView = new Intent(getApplicationContext(), UpdateCourseNotice.class);
			editView.putExtra("subject", subject);
			editView.putExtra("content", viewHolder.subjectHtml);
			editView.putExtra("noticeId", noticeid);
			editView.putExtra("subjectId", subjectID);
			startActivity(editView);
			finish();
		}

		return true;
	}

	class LoadCourseNotices extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(CourseNotices.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}

		//getting all notices from URL
		protected String doInBackground(String... args) {
			//Building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("courseid", subjectID));
			params.add(new BasicNameValuePair("userid", userid));

			//getting JSON string URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "mycoursenotices.php", "GET", params);

			//check log for JSON result
			Log.d("Notices: ", json.toString());

			try {
				jArray = json.getJSONArray("notices");

				//looping through the JSONArray
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject n = jArray.getJSONObject(i);

					//storing each JSON item in variable
					String nid = n.getString("id");
					String subject = n.getString("subject");
					String content = n.getString("content");

					//creating a HashMap
					HashMap<String, Object> map = new HashMap<String, Object>();

					//adding each child node to HashMap
					map.put("nid", nid);
					map.put("subject", subject);
					map.put("content", content);

					//adding the HashMap to ArrayList
					noticeList.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String filepath) {
			//dismiss progress dialog after execution
			dialog.dismiss();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// updating JSON data into ListView
					ListAdapter adapter = new SimpleAdapter(CourseNotices.this, noticeList, R.layout.notice_list, new String[]{"id", "nid", "subject", "content"}, new int[]{R.id.courseid, R.id.noticeid, R.id.subject, R.id.content});
					setListAdapter(new BaseAdapter() {
						@Override
						public int getCount() {
							return noticeList.size();
						}

						@Override
						public HashMap<String, Object> getItem(int position) {
							return noticeList.get(position);
						}

						@Override
						public long getItemId(int position) {
							return position;
						}

						@Override
						public View getView(int position, View convertView, ViewGroup parent) {
							ViewHolder viewHolder;
							if (convertView == null) {
								LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
								convertView = layoutInflater.inflate(R.layout.notice_list, null);
								viewHolder = new ViewHolder();
								viewHolder.courseid = (TextView) convertView.findViewById(R.id.courseid);
								viewHolder.noticeid = (TextView) convertView.findViewById(R.id.noticeid);
								viewHolder.subject = (TextView) convertView.findViewById(R.id.subject);
								viewHolder.content = (TextView) convertView.findViewById(R.id.content);
								convertView.setTag(viewHolder);
							} else {
								viewHolder = (ViewHolder) convertView.getTag();
							}
							HashMap<String, Object> item = getItem(position);
							//viewHolder.courseid.setText(item.get("id").toString());
							viewHolder.noticeid.setText(item.get("nid").toString());
							viewHolder.subject.setText(item.get("subject").toString());
							viewHolder.content.setText(Html.fromHtml(item.get("content").toString()));
							viewHolder.subjectHtml=item.get("content").toString();
							return convertView;
						}
					});
				}
			});
		}
	}

	private static class ViewHolder {

		TextView courseid;
		TextView noticeid;
		TextView subject;
		TextView content;
		String subjectHtml;
	}

	class DeleteNotice extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(CourseNotices.this);
			dialog.setMessage("Deleting...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}

		protected String doInBackground(String... args) {
			//Building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			//name in the basicnamevalue pair should always be your $_GET['name'] in php
			params.add(new BasicNameValuePair("id", noticeid));

			//getting the SQL query to work via JSON
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "deletenotices.php", "GET", params);

			Log.d("JSON Object", "getting - working");
			Log.d("status: ", json.toString());

			try {
				result = json.getInt("success");
				Log.d("getting success", "working");

				msg = json.getString("message");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String filepath) {
			if (result == 1) {
				Toast.makeText(CourseNotices.this, msg, Toast.LENGTH_LONG).show();

				finish();
				startActivity(getIntent());
			}

			if (result == 0) {
				Toast.makeText(CourseNotices.this, msg, Toast.LENGTH_LONG).show();
			}

			dialog.dismiss();
		}
	}
}
