package com.moodleapp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CalendarActivity extends Activity implements OnClickListener {

	//assign a tag for the activity
	private static final String CALENDAR_TAG = "CalendarActivity";
	
	//declare properties of this class
	private Button btnPrevMonth;
	private TextView tvCurMonth;
	private Button btnNextMonth;
	private TextView tvSelectedDate;
	private Button btnAddEvent;
	private GridView gvCalendar;
	private LinearLayout llcalendarDay;
	
	//instance of GridCellAdapter inner class
	private GridCellAdapter adapter;
	
	//instance of calendar
	private Calendar _calendar;
	
	//to access month and year
	private int month, year;
	
	//date template to display
	private static final String dateTemplate = "MMMM yyyy";
	
	//to access dates of the month haveing events
	private int[] intEventDates = null;
	
	//instance of SessionManager Class
	SessionManager session;
	
	//to access the current user's id
	public String userId;
	
	//to access the current user's role-privilege level
	public String roleName;
	
	//instanse of JSON_Parser class
	JSON_Parse jParser = new JSON_Parse();
	
	//json array to get the data from db
	JSONArray jArray = null;
		
	//to check if the adding a event is successful
	boolean success;
	
	ProgressDialog pDialog;
	
	//to get the event dates
	int Events[] = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calendar);

		//getting userid of the current user from sessions
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		userId = user.get(SessionManager.KEY_USERID);
		
		//getting role of the current user from sessions
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> role = session.getUserDetails();
		roleName = role.get(SessionManager.KEY_ROLE);
		
		//declare a GridView for the calendar
		gvCalendar = (GridView) this.findViewById(R.id.gvCalendar);
				
		//call initCalendar() method
		initCalendar();
		
		registerForContextMenu(gvCalendar);
		
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);

	    if(roleName.equals("Teacher"))
	    {
	    	menu.add(0, 1, 1, "Add an event");
	    }
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    
	    if(item.getItemId() == 1)
	    {
	    	Intent i = new Intent(getApplicationContext(),AddEventActivity.class);
			startActivity(i);
	    }
		return true;
	    
	    
	}
	//get a array of dates which having events
	public int[] GetEventArray(JSONObject jObj) {
		
		String strSuccess = "";
		
		try 
		{	
			strSuccess = jObj.getString("success");
			
			if (strSuccess != null) {
				if (strSuccess.equals("true")) {
				
					//accessing the dates in database
					JSONArray jObjArr = jObj.getJSONArray("dates"); 
		
					//retrieve the dates and store in an array
					Events = new int [jObjArr.length()];
					for (int intIndex = 0;intIndex < jObjArr.length();intIndex++)
					{
						Events[intIndex] =  ((JSONObject)jObjArr.get(intIndex)).getInt("date");
					}
				
					return Events;
				}
				else
					return null;
			}
			else
				return null;
		}
		catch (Exception ex)
		{
			return null;
		}
	}
	//initial calendar view - for everyone
	public void initCalendar()
	{
		//get the current month and current year
		_calendar = Calendar.getInstance(Locale.getDefault());
		month = _calendar.get(Calendar.MONTH) + 1;
		year = _calendar.get(Calendar.YEAR);
		Log.d(CALENDAR_TAG, "Calendar Instance:= " + "Month: " + month + " " + "Year: "
				+ year);

		//properties of the view
		tvSelectedDate = (TextView) this.findViewById(R.id.tvSelectedDate);
		tvSelectedDate.setText("Selected: ");

		btnPrevMonth =(Button) this.findViewById(R.id.btnPrevMonth);
		btnPrevMonth.setOnClickListener((OnClickListener) this);
		
		tvCurMonth = (TextView) this.findViewById(R.id.tvCurrentMonth);
		tvCurMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));

		btnNextMonth = (Button) this.findViewById(R.id.btnNextMonth);
		btnNextMonth.setOnClickListener((OnClickListener) this);
		
		btnAddEvent = (Button) this.findViewById(R.id.btnAddEvent);
		btnAddEvent.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				//make the access to add event only to a lecturer
				if(session.isLoggedIn() == false)
				{
					Toast.makeText(getApplicationContext(), "You are not allowed to add an event!!!", Toast.LENGTH_LONG).show();
				}
				else
				{
					if(roleName.equals("Teacher"))
					{
						//navigate To AddEventActivity if the user is a lecturer
						Intent i = new Intent(getApplicationContext(),AddEventActivity.class);
						startActivity(i);
					}
					else
					{
						//display an errors for the other users who does not have access to add events
						Toast.makeText(getApplicationContext(), "You are not allowed to add an event!!!", Toast.LENGTH_LONG).show();
					}
				}
			}
		});

		//declare a GridView for the calendar
		//gvCalendar = (GridView) this.findViewById(R.id.gvCalendar);

		// pass the array of date which have events
		//intEventDates = GetEventArray("{'dates':[{'date':'03'},{'date':'20'},{'date':'20'},{'date':'29'},{'date':'29'}],'success':true}");
		
		new LoadEventDates().execute();
		
	}
	
	//put dates on adapter
	private void setGridCellAdapterToDate(int month, int year, int[] intEventDays) {
		
		adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year, intEventDays);
		
		//get the day month and year
		_calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
		
		//change the text of the textview for viewing current month
		tvCurMonth.setText(DateFormat.format(dateTemplate,
				_calendar.getTime()));
		//refresh the view of the caledar as it is changed
		adapter.notifyDataSetChanged();
		
		//set the data for grid view
		gvCalendar.setAdapter(adapter);
	}
	
	public void onClick(View v)
	{
		//click event for the button for previous month
		if (v == btnPrevMonth) 
		{
			//if the selected month is january
			if (month <= 1) 
			{
				//set the previous month as 12
				month = 12;
				//decrement the current year to get previous year
				year--;
			} 
			else 
			{
				//decrement the month
				month--;
			}
			Log.d(CALENDAR_TAG, "Setting Prev Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
			
			//retireve the date which having events for the selected month
			//intEventDates = GetEventArray("{'dates':[{'date':'03'},{'date':'20'},{'date':'20'},{'date':'29'},{'date':'29'}],'success':true}");
			//intEventDates = GetEventArray("http://10.0.2.2/retrieveEvents.php");
			
			//load event dates from the db
			new LoadEventDates().execute();
			
		}
		
		//click event for the button for next month
		if (v == btnNextMonth) 
		{
			//check if the month is december
			if (month > 11)
			{
				//set the next month as january
				month = 1;
				//increment the current month to get next month
				year++;
			} 
			else 
			{
				//increment month
				month++;
			}
			Log.d(CALENDAR_TAG, "Setting Next Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
			//retireve the date which having events for the selected month
			//intEventDates = GetEventArray("{'dates':[{'date':'03'},{'date':'20'},{'date':'20'},{'date':'29'},{'date':'29'}],'success':true}");
			//intEventDates = GetEventArray("http://10.0.2.2/retrieveEvents.php");
			
			//load events from the db
			new LoadEventDates().execute();
			
			
			//specify the dates of month
			//setGridCellAdapterToDate(month, year, intEventDates);
		}
	}
	
	@Override
	public void onDestroy() {
		Log.d(CALENDAR_TAG, "Destroying View ...");
		super.onDestroy();
	}
	

		// Inner Class
		public class GridCellAdapter extends BaseAdapter implements OnClickListener {
			//assing a tag for the inner class
			private static final String tag = "GridCellAdapter";
			
			//to access the current context
			private final Context _context;
			
			//instance of gridCellAdapter
			private GridCellAdapter Celladapter = null;
			
			//
			private final List<DateProperties> list;
			private static final int DAY_OFFSET = 1;
			
			//declare an array to get the strings of weekdays
			private final String[] weekdays = new String[] { "Sun", "Mon", "Tue",
															 "Wed", "Thu", "Fri", "Sat" };

			//declare an array to get the strings of months
			private final String[] months = { "January", "February", "March",
											"April", "May", "June", 
											"July", "August", "September",
											"October", "November", "December" };
			
			//array for days of month
			private final int[] daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30,
					31, 30, 31 };
			
			//number of days in a month
			private int daysInMonth;
			
			//current day of the month
			private int currentDayOfMonth;
			
			//current week day of the month
			private int currentWeekDay;
			
			//to dispaly dates on grid view
			private Button gridcell;
			
			//to access event dates
			private int[] intEventDays = null;
			
			//to access current month
			private int CurrentMonth = 0;
			
			private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");

			// Days in Current Month
			public GridCellAdapter(Context context, int textViewResourceId, int month, int year, int[] intEventDays) {
				super();
				
				this._context = context;
				this.Celladapter = this;
				this.intEventDays = intEventDays;
				this.CurrentMonth = month;
				
				this.list = new ArrayList<DateProperties>();
				Log.d(tag, "==> Passed in Date FOR Month: " + month + " "
						+ "Year: " + year);
				//get instances of the calendar
				Calendar calendar = Calendar.getInstance();
				setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
				setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
				Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
				Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
				Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());

				// method for Print Month
				printMonth(month, year);
			}
			
			//to get the string for the month
			private String getMonthAsString(int i) {
				return months[i];
			}

			//to get the string for weekday
			private String getWeekDayAsString(int i) {
				return weekdays[i];
			}

			//get nuber of days in a month
			private int getNumberOfDaysOfMonth(int i) {
				return daysOfMonth[i];
			}

			//get the position of a particular item in grid view
			public DateProperties getItem(int position) {
				return list.get(position);
			}

			//get the number of items in the current grid view
			@Override
			public int getCount() {
				return list.size();
			}

			//printing the month
			private void printMonth(int mm, int yy) {
				Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
				int trailingSpaces = 0;
				int daysInPrevMonth = 0;
				int prevMonth = 0;
				int prevYear = 0;
				int nextMonth = 0;
				int nextYear = 0;

				//get the index of the current month - month is 0  based
				int currentMonth = mm - 1;
				//get the name of the current month
				String currentMonthName = getMonthAsString(currentMonth);
				//get the number of days in current month
				daysInMonth = getNumberOfDaysOfMonth(currentMonth);

				Log.d(tag, "Current Month: " + " " + currentMonthName + " having "
						+ daysInMonth + " days.");

				//get the instance of calendar
				GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
				Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

				//if the current month is december
				if (currentMonth == 11) {
					//set the preveious month
					prevMonth = currentMonth - 1;
					//get the number of days in previous month
					daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
					//set the next month as january
					nextMonth = 0;
					//set the previous year as it is
					prevYear = yy;
					//increment the year by 1 to get next year
					nextYear = yy + 1;
					Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:"
							+ prevMonth + " NextMonth: " + nextMonth
							+ " NextYear: " + nextYear);
				} 
				//if the current month is january
				else if (currentMonth == 0) 
				{
					//set the preveious month as december
					prevMonth = 11;
					//to get the previous year increment current year
					prevYear = yy - 1;
					//set the next year as it is
					nextYear = yy;
					//get the number of days in previous month
					daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
					//get the next month
					nextMonth = 1;
					Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:"
							+ prevMonth + " NextMonth: " + nextMonth
							+ " NextYear: " + nextYear);
				} else
				{
					//set previous month 
					prevMonth = currentMonth - 1;
					//set next month
					nextMonth = currentMonth + 1;
					//set next year
					nextYear = yy;
					//set previous years
					prevYear = yy;
					//set number of days in the previous month
					daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
					Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:"
							+ prevMonth + " NextMonth: " + nextMonth
							+ " NextYear: " + nextYear);
				}

				//get the current weekday
				int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
				trailingSpaces = currentWeekDay;

				Log.d(tag, "Week Day:" + currentWeekDay + " is "
						+ getWeekDayAsString(currentWeekDay));
				Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
				Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

				//if the currently viewing year is a leap year
				if (cal.isLeapYear(cal.get(Calendar.YEAR)))
					if (mm == 2)
						++daysInMonth;
					else if (mm == 3)
						++daysInPrevMonth;

				// Trailing Month days
				for (int i = 0; i < trailingSpaces; i++) {
					Log.d(tag,
							"PREV MONTH:= "
									+ prevMonth
									+ " => "
									+ getMonthAsString(prevMonth)
									+ " "
									+ String.valueOf((daysInPrevMonth
											- trailingSpaces + DAY_OFFSET)
											+ i));
					
					DateProperties objDateProperties = new DateProperties();
					objDateProperties.strDisplayDate = String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i)
							+ "-GREY"
							+ "-"
							+ getMonthAsString(prevMonth)
							+ "-"
							+ prevYear;
					objDateProperties.strDate = String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "/" + prevMonth + "/" + prevYear;
					list.add(objDateProperties);
				}

				// Current Month Days
				for (int i = 1; i <= daysInMonth; i++) 
				{
					Log.d(currentMonthName, String.valueOf(i) + " "
							+ getMonthAsString(currentMonth) + " " + yy);
					
					if (i == getCurrentDayOfMonth()) 
					{
						DateProperties objDateProperties = new DateProperties();
						objDateProperties.strDisplayDate = String.valueOf(i) + "-BLUE" + "-"
								+ getMonthAsString(currentMonth) + "-" + yy;
						objDateProperties.strDate = String.valueOf(i) + "/" + currentMonth + "/" + yy;
						list.add(objDateProperties);
						
					}
					else 
					{
						DateProperties objDateProperties = new DateProperties();
						objDateProperties.strDisplayDate = String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy;
						objDateProperties.strDate = String.valueOf(i) + "/" + currentMonth + "/" + yy;
						list.add(objDateProperties);
					}
				}

				// Loading Month days
				for (int i = 0; i < list.size() % 7; i++) {
					Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
					DateProperties objDateProperties = new DateProperties();
					objDateProperties.strDisplayDate = String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear;
					objDateProperties.strDate = String.valueOf(i + 1) + "/" + nextMonth + "/" + nextYear;
					list.add(objDateProperties);
				}
			}

			//get the current position of the grid view
			@Override
			public long getItemId(int position) {
				return position;
			}

			//make changes to the current view
			@Override
			public View getView(int position, View convertView, ViewGroup parent) 
			{
				
				View row = convertView;
				if (row == null) 
				{
					LayoutInflater inflater = (LayoutInflater) _context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					row = inflater.inflate(R.layout.madhu_calendar_gridcell, parent, false);
				}

				// Get a reference to the Day gridcell
				gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
				gridcell.setOnClickListener(this);

				// ACCOUNT FOR SPACING

				Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
				String[] day_color = list.get(position).strDisplayDate.split("-");
				String theday = day_color[0];
				String themonth = day_color[2];
				String theyear = day_color[3];


				// Set the Day GridCell
				gridcell.setText(theday);
				gridcell.setTag(list.get(position));

				if (day_color[1].equals("GREY")) 
				{
					gridcell.setTextColor(getResources()
							.getColor(R.color.lightgray));
				}
				if (day_color[1].equals("WHITE")) 
				{
					gridcell.setTextColor(getResources().getColor(
							R.color.blue));
				}
				if (day_color[1].equals("BLUE"))
				{
					gridcell.setTextColor(getResources().getColor(R.color.orrange));
				}
				
				// Set color of the selected day
				if (list.get(position).bolIsSelect) 
				{
					gridcell.setBackgroundColor(getResources().getColor(R.color.gray));
				}
				else 
				{	
					//show logged users the dates which are allocated for events 
					if(session.isLoggedIn() == true)
					{
						
						if (intEventDays != null) {
							
							// Set event color
							for (int intIndex = 0; intIndex < intEventDays.length;intIndex++) 
							{
								if (theday.equals(String.valueOf(intEventDays[intIndex])) 
										&& getMonthAsString(CurrentMonth - 1).equals(themonth))
								{
									gridcell.setBackgroundColor(getResources().getColor(R.color.darkorrange));
									
								}
								else
								{
									gridcell.setBackgroundColor(getResources().getColor(R.color.white));
								}
							}
						}
						else {
							gridcell.setBackgroundColor(getResources().getColor(R.color.white));
						}
					}
					
				}
				
				//show logged users the dates which are allocated for events 
				if(session.isLoggedIn() == true)
				{
					if (intEventDays != null) {
						
						// Set event color
						for (int intIndex = 0; intIndex < intEventDays.length;intIndex++) 
						{
							if (theday.equals(String.valueOf(intEventDays[intIndex])) && getMonthAsString(CurrentMonth - 1).equals(themonth))
							{
								gridcell.setBackgroundColor(getResources().getColor(R.color.darkorrange));
							}
						}
					}
				}
				
				return row;
			}

			//when click on a date
			@Override
			public void onClick(View view) {
				
				DateProperties date_month_year = (DateProperties) view.getTag();
				
				String[] day_color = date_month_year.strDisplayDate.split("-");
				String theday = day_color[0];
				String themonth = day_color[2];
				String theyear = day_color[3];
				
				//get the selected date to specify
				AddEventActivity.strSelectedToDate = date_month_year.strDate;
				AddEventActivity.strSelectedFromDate = date_month_year.strDate;
				
				String strDate_month_year = theday + "-" + themonth + "-" + theyear;
				tvSelectedDate.setText("Selected Date: " + strDate_month_year);
				Log.e("Selected date", strDate_month_year);
				
				try 
				{
					view.setBackgroundColor(getResources().getColor(R.color.gray));
					Date parsedDate = dateFormatter.parse(strDate_month_year);
					Log.d(tag, "Parsed Date: " + parsedDate.toString());

				} 
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
				
				// Clear previous selected date
				for (int intIndex = 0;intIndex < list.size();intIndex++) {
					list.get(intIndex).bolIsSelect = false;
				}
				date_month_year.bolIsSelect = true;	
				
				Celladapter.notifyDataSetChanged();
			}

			//get the current day of the month
			public int getCurrentDayOfMonth() {
				return currentDayOfMonth;
			}

			//set current day of the month
			private void setCurrentDayOfMonth(int currentDayOfMonth) {
				this.currentDayOfMonth = currentDayOfMonth;
			}

			//set current weekday
			public void setCurrentWeekDay(int currentWeekDay) {
				this.currentWeekDay = currentWeekDay;
			}

			//get current week day
			public int getCurrentWeekDay() {
				return currentWeekDay;
			}
			
		}
		
		class LoadEventDates extends AsyncTask<String, String, String> {
		   	 
	        //Before starting background thread Show Progress Dialog
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(CalendarActivity.this);
	            pDialog.setMessage("Loading Details. Please wait...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(false);
	            pDialog.show();
	        }
	 
	        //get all the details needed to compare with db
	        protected String doInBackground(String... args) {
	        	
	            // Building Parameters
	        	List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("month", String.valueOf(month)));
				params.add(new BasicNameValuePair("year",String.valueOf(year)));
				
			    
	            // getting JSON string from URL
	            JSONObject json = jParser.makeHttpRequest(session.strUrl + "retrieveEvents.php", "GET", params);
	 
	            try 
	            {
	            	
	            	intEventDates = GetEventArray(json);
	            	//retrieve the data from database
//	            	Events = new int [json.length()];
//	            	Events = intEventDates;
//	    			for (int intIndex = 0;intIndex < json.length();intIndex++)
//	    			{
//	    				//accessing the dates in database
//	    				JSONArray jObjArr = json.getJSONArray("dates");
//	    				
//	    				intEventDates[intIndex] =  ((JSONObject)jObjArr.get(intIndex)).getInt("date");
//	    				
//	    			}
	            } 
	            catch (Exception e) 
	            {
	                e.printStackTrace();
	            }
	 
	            return null;
	        }
	        
	        protected void onPostExecute(String file_url) {
	        	
	        	  runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                    	  
                    	  try {
                    	  
                    		  // dismiss the dialog after getting all products
                    		  pDialog.dismiss();	
				
                    		  //specify the dates of month
                    		  setGridCellAdapterToDate(month, year, intEventDates);
                    	  }
                    	  catch (Exception ex) {
                    		  
                    	  }
                      }
                  });
			}
		}	
}
