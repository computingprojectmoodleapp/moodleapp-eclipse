package com.moodleapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import hu.scythe.droidwriter.DroidWriterEditText;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateCourseNotice extends Activity {
	
	String subjectID;
	String coursename;
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	EditText txtSubject;
	AlertDialogManager alert = new AlertDialogManager();
	private DroidWriterEditText droidWriterEditText;
	private Button btnClear;
	
	SessionManager session;
	String userID;

	String noticeId;
	String subject;
	String content;
	int result;
	
	String msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_course_notice);
		
		Intent intent = getIntent();
		//getting the courseid from the previous intent
		subjectID = intent.getStringExtra("subjectId");
		coursename = intent.getStringExtra("name");
		noticeId = intent.getStringExtra("noticeId");
		subject = intent.getStringExtra("subject");
		content = intent.getStringExtra("content");

		//getting userid from sessions
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		
		userID = user.get(SessionManager.KEY_USERID);
		 
		txtSubject = (EditText) findViewById(R.id.editHeader);

		//initializing droidWriterEditText, a customized third party EditText
		droidWriterEditText = (DroidWriterEditText) findViewById(R.id.notice);

		//setting up toggle button for BOLD
		droidWriterEditText.setBoldToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleBold));

		//setting up toggle button for ITALIC
		droidWriterEditText.setItalicsToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleItalic));

		//setting up toggle button for UNDERLINE
		droidWriterEditText.setUnderlineToggleButton((android.widget.ToggleButton) findViewById(R.id.toggleUnderline));

		//setting up button for clear text
		droidWriterEditText.setClearButton(btnClear = (Button) findViewById(R.id.btnClear));

		//setup subject
		txtSubject.setText(subject);

		//setup html string to droidWriterEditText
		droidWriterEditText.setText(Html.fromHtml(content));

		Button btnAdd = (Button) findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				subject = txtSubject.getText().toString();

				//retrieve text in droidWriterEditText as a html and assign it to field variable content
				content = Html.toHtml(droidWriterEditText.getSpannedText());
				if((subject.trim().isEmpty()) || (content.trim().isEmpty())){
					alert.showAlertDialog(UpdateCourseNotice.this, "Error!", "Please fill all fields.", false);
				}
				else{
					//calling the background thread to execute
					//calls to webService which is programmed to update course notice in remote centralized database
					new UpdateNotice().execute();
				}
				
			}
		});

		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//clear edited texts
				clearTexts();
			}
		});
	
	}
	
	public void clearTexts(){
		txtSubject.setText("");
		droidWriterEditText.setText("");
	}
	
	class UpdateNotice extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
		
		protected String doInBackground(String...args){
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("subject", subject));
			params.add(new BasicNameValuePair("content", content));
			params.add(new BasicNameValuePair("id", noticeId));

			Log.d("doInbackground", "working");
			
			//getting JSON string URL
			//calls to webService which is programmed to update course notice in remote centralized database
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "EditNotices.php", "GET", params);

			//check log cat for JSON response
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				Log.d("getting success", "working");
				
				msg = json.getString("message");
				
			}catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		protected void onPostExecute(String filepath){
		
			if (result == 1){
				Toast.makeText(UpdateCourseNotice.this, msg, Toast.LENGTH_SHORT).show();
				clearTexts();
				Intent intent = new Intent(getApplicationContext(), CourseNotices.class);
				intent.putExtra("id", subjectID);
				intent.putExtra("name", coursename);
				startActivity(intent);
				finish();
			}
			if (result == 0) {
				alert.showAlertDialog(UpdateCourseNotice.this, "Oops!", msg, false);
			}
			
		}
	}
		
	}
	
