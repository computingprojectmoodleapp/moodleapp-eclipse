package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddForumPost extends Activity{
	
	String subjectID;
	String forumID;
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	EditText txtSubject, txtContent;
	
	AlertDialogManager alert = new AlertDialogManager();
	
	SessionManager session;
	String userID;
	
	String subject;
	String content;
	int result;
	String msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_forum_post);
		
		Intent intent = getIntent();
		//getting course ID from previous intent
		subjectID = intent.getStringExtra("id");
		forumID = intent.getStringExtra("forumID");
		
		//getting user ID from session
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		userID = user.get(SessionManager.KEY_USERID);
		
		txtSubject = (EditText) findViewById(R.id.editSubject);
		txtContent = (EditText) findViewById(R.id.editMessage);
		
		Button btnAdd = (Button) findViewById(R.id.btnAddPost);
		btnAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				subject = txtSubject.getText().toString();
				content = txtContent.getText().toString();
				
				if((subject.equals("")) || (content.equals(""))){
					alert.showAlertDialog(AddForumPost.this, "Oops!", "Please fill all fields.", false);
				}
				else {
					//calling the background thread to execute
					new AddPost().execute();
			
				}
				
			}
		});
		
		Button btnClear = (Button) findViewById(R.id.btnClearData);
		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// clear the text boxes
				clearTexts();
				
			}
		});
	}
	
	public void clearTexts(){
		txtSubject.setText("");
		txtContent.setText("");
	}
	
	class AddPost extends AsyncTask<String, String, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		protected String doInBackground(String...args){
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("course", subjectID));
			params.add(new BasicNameValuePair("forum", forumID));
			params.add(new BasicNameValuePair("name", subject));
			params.add(new BasicNameValuePair("message", content));
			params.add(new BasicNameValuePair("userid", userID));
			
			Log.d("doInBackground", "working");
			
			//getting JSON String from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "addforumposts.php", "GET", params);
			
			//check log for JSON response
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				msg = json.getString("message");
				
			} catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String filepath) {
			if(result == 1){
				//alert.showAlertDialog(AddForumPost.this, "Done!", msg, false);
				Toast.makeText(AddForumPost.this, msg, Toast.LENGTH_SHORT).show();
				finish();
				
				Intent intent = new Intent(AddForumPost.this, DiscussionTopics.class);
				intent.putExtra("id", subjectID);
				intent.putExtra("forumID", forumID);
				startActivityForResult(intent, 100);
				
			}
			if (result == 0){
				alert.showAlertDialog(AddForumPost.this, "Oops!", msg, false);
			}
		}
	}

}
