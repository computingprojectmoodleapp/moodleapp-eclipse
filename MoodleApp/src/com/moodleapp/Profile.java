package com.moodleapp;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile extends Activity {
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
		
		// Session Manager Class
	SessionManager session;
		
		//to get user id and name
	String uid, name;
		
		//to get the menu item to store user's name
	MenuItem itemUser;
	
	String itemName;
	
	//String userID;
	
	private ProgressDialog pDialog;
	private ActionBar actionBar;
	
	JSON_Parse jParser = new JSON_Parse();
	 
    //ArrayList<HashMap<String, String>> productsList;
 
    private String url_all_products = session.strUrl + "UserData.php";
 
    JSONObject json;
    
    String userName;
    String fullName;
    String email;
    String city;
    String country;
    String picture;
    
    Bitmap bitmap;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_layout);
		
		//object reference to session manager class
  		session = new SessionManager(getApplicationContext());
  		
        
  		//check login
      	//session.checkLogin();
     
          // get user data from session
          HashMap<String, String> user = session.getUserDetails();
          
          actionBar = getActionBar();

  		// Hide the action bar title
          actionBar.setDisplayHomeAsUpEnabled(true);
          
          //store user id
          uid = user.get(SessionManager.KEY_USERID);
          
          //store username
          name = user.get(SessionManager.KEY_NAME);
          
          Button buttonLoadImage = (Button) findViewById(R.id.btnEditProfile);
          buttonLoadImage.setOnClickListener(new View.OnClickListener() {
               
              @Override
              public void onClick(View arg0) {
                   
            	  Intent profile = new Intent(getApplicationContext(),EditProfile.class);
            	  profile.putExtra("data", json.toString());
            	  startActivity(profile);
              }
          });
                          
        new LoadAllProducts().execute();
	}
	
	class LoadAllProducts extends AsyncTask<String, String, String> {
   	 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
        	List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("userID", uid));
            // getting JSON string from URL
            
            json = jParser.makeHttpRequest(url_all_products, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
            	int success = json.getInt("success");
 
            	if (success == 1) {
            	   userName = json.getString("username");
                   fullName = json.getString("firstname")+ " " + json.getString("lastname");
                   email = json.getString("email");
                   city = json.getString("city");
                   country = json.getString("country");
                   picture = json.getString("picture");
                } else {
                	Log.e("Error", "Load User Data");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (picture.equals("1"))
        	{
        		bitmap = DownloadImage(session.strUrl + "user/pix.php/" + uid + "/f1.jpg"); //../moodledata/user/0/2/f1.jpg");("http://10.0.2.2/Chrysanthemum.jpg"); // ProfilePicture.php?userID=uid);
        	}
        	else {
				bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile);
			}
            return null;
        }
        
        protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			ImageView imgProPic = (ImageView) findViewById(R.id.profile);
			imgProPic.setImageBitmap(bitmap);
			TextView tvFullName = (TextView) findViewById(R.id.lblName);
			tvFullName.setText(fullName);
			TextView tvUserName = (TextView) findViewById(R.id.lblUserName);
			tvUserName.setText(userName);
			TextView tvEmail = (TextView) findViewById(R.id.lblEmail);
			tvEmail.setText(email);
			TextView tvCity = (TextView) findViewById(R.id.lblCityTown);
			tvCity.setText(city);
			TextView tvCountry = (TextView) findViewById(R.id.lblCountry);
			tvCountry.setText(country);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					//ListAdapter adapter = new SimpleAdapter(Profile.this, productsList, R.layout.list_item, new String[] {TAG_PID, TAG_NAME, "reference"}, new int[] { R.id.id, R.id.name, R.id.reference });
					// updating listview
					//setListAdapter(adapter);
				}
			});

		}
	}
	
	private InputStream OpenHttpConnection(String urlString)
		    throws IOException
		    {
		        InputStream in = null;
		        int response = -1;
		                 
		        URL url = new URL(urlString);
		        URLConnection conn = url.openConnection();
		                   
		        if (!(conn instanceof HttpURLConnection))                   
		            throw new IOException("Not an HTTP connection");
		          
		        try{
		            HttpURLConnection httpConn = (HttpURLConnection) conn;
		            httpConn.setAllowUserInteraction(false);
		            httpConn.setInstanceFollowRedirects(true);
		            httpConn.setRequestMethod("GET");
		            httpConn.connect();
		  
		            response = httpConn.getResponseCode();               
		            if (response == HttpURLConnection.HTTP_OK) {
		                in = httpConn.getInputStream();                               
		            }                   
		        }
		        catch (Exception ex)
		        {
		            throw new IOException("Error connecting");          
		        }
		        return in;   
		    }
		    private Bitmap DownloadImage(String URL)
		    {      
		        Bitmap bitmap = null;
		        InputStream in = null;      
		        try {
		            in = OpenHttpConnection(URL);
		            bitmap = BitmapFactory.decodeStream(in);
		            in.close();
		        } catch (IOException e1) {
		            // TODO Auto-generated catch block
		            e1.printStackTrace();
		        }
		        return bitmap;              
		    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
    	getMenuInflater().inflate(R.menu.icon_menu, menu);
    	
    	if(session.isLoggedIn() == true)
    	{
    		menu.add(0, 1, Menu.NONE, name);
    		menu.add(0, 4, Menu.NONE, "Refresh");
    		menu.add(0, 2, Menu.NONE, "Logout");
    		getMenuInflater().inflate(R.menu.logged_in, menu);
    	}
    	else
    	{
    		menu.add(0, 3, Menu.NONE, "Login");
    	}	
	
		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	int id = item.getItemId();
    	
    	if (android.R.id.home == id) {
			Intent profile = new Intent(getApplicationContext(), MainTabs.class);
			startActivity(profile);
			return true;
		}
		
		if (id == 1) {
			Intent profile = new Intent(getApplicationContext(),Profile.class);
			startActivity(profile);
			return true;
		}
		
		if(id == 2){
			session.logoutUser();
			return true;
		}
		
		if(id == 3)
		{
			Intent loginAct = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(loginAct);
			return true;
		}
		
		if(id == 4)
    	{
    		finish();
    		startActivity(getIntent());
    		return true;
    	}
		
		if (id == R.id.action_mail)
		{
			
		}
		
		if(id == R.id.ic_action_event)
		{
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(), CalendarActivity.class);
			startActivity(calendar);
		}
	
	return super.onOptionsItemSelected(item);
    }

}
