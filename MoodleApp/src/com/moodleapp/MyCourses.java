package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


public class MyCourses extends ListActivity{

	int result;
	String msg;
	String userID;
	String username;
	
	private ProgressDialog dialog;
	JSON_Parse jParser = new JSON_Parse();
	ArrayList<HashMap<String, String>> courselist;
	JSONArray jArray = null;
	
	AlertDialogManager alert = new AlertDialogManager();
	
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mycourses_list);
		
		//object reference to session manager class
  		session = new SessionManager(getApplicationContext());
  		HashMap<String, String> user = session.getUserDetails();
  		username = user.get(SessionManager.KEY_NAME);
		
		Intent intent = getIntent();
		//getting user ID from previous intent
		userID = intent.getStringExtra("uid");
		
		courselist = new ArrayList<HashMap<String,String>>();
		
		//loading all courses in the background thread
		new LoadMyCourses().execute();
		Log.d("LoadMyCourses: ", "working");
		
		ListView list = getListView();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String subjectID = ((TextView)view.findViewById(R.id.courseid)).getText().toString();
				String subject = ((TextView)view.findViewById(R.id.course)).getText().toString();
				
				Intent intent = new Intent(getApplicationContext(), SubjectDetails.class);
				intent.putExtra("id", subjectID);
				intent.putExtra("name", subject);
				
				startActivityForResult(intent, 100);
				
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mycourses_menu, menu);
		menu.add(0, 1, Menu.NONE, username);
		menu.add(0, 2, Menu.NONE, "Logout");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == 1){
			
		}
		if (item.getItemId() == 2){
			session.logoutUser();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	
	class LoadMyCourses extends AsyncTask<String, String, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(MyCourses.this);
			dialog.setMessage("Loading...");
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
		}
		
		//getting all courses from JSON URL
		protected String doInBackground(String...args){
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("userid", userID));
			
			//getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "mycourses.php", "GET", params);
			
			//check log for JSON result
			Log.d("status: ", json.toString());
			
			try{
				jArray = json.getJSONArray("courses");
				
				//looping through the JSONArray
				for(int i=0; i< jArray.length(); i++){
					JSONObject course = jArray.getJSONObject(i);
					
					//storing each JSON item in variables
					String courseid = course.getString("id");
					String coursename = course.getString("fullname");
					
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("courseid", courseid);
					map.put("course", coursename);
					
					//adding the HashMap to ArrayList
					courselist.add(map);
				}
				
				result = json.getInt("success");
				msg = json.getString("message");
				
			}catch(JSONException e){
				e.printStackTrace();
			}
			return null;
		}
		
		
		@Override
		protected void onPostExecute(String filepath) {
			//dismiss progress dialog after loading
			dialog.dismiss();
			
			if(result == 1){
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						//updating JSON data into the ListView
						ListAdapter adapter = new SimpleAdapter(MyCourses.this, courselist, R.layout.mycourses_item, new String[] {"courseid","course"}, new int[] {R.id.courseid, R.id.course});
						setListAdapter(adapter);
						
					}
				});
			}
			
			if(result == 0){
				alert.showAlertDialog(MyCourses.this, "Oops!", "You have not enrolled in any courses.", false);
			}
			
		}
	}
}
