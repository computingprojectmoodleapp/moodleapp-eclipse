package com.moodleapp;

import java.util.HashMap;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;


@SuppressWarnings("deprecation")
public class MainTabs extends TabActivity {
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Session Manager Class
	SessionManager session;
	
	//to get user id and name
	String uid, name;
	
	//to get the menu item to store user's name
	MenuItem itemUser;
	
	TabHost tabHost;
	private ActionBar actionBar;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main_tabs_layout);
        
      //object reference to session manager class
      		session = new SessionManager(getApplicationContext());
      		
      		//check login
          	//session.checkLogin();
         
              // get user data from session
              HashMap<String, String> user = session.getUserDetails();
              
              actionBar = getActionBar();

      		// Hide the action bar title
              actionBar.setDisplayHomeAsUpEnabled(true);
              
              //store user id
              uid = user.get(SessionManager.KEY_USERID);
              
              //store username
              name = user.get(SessionManager.KEY_NAME);
              
             /* //message to indicate user's login status
      	    Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), 
      	        		Toast.LENGTH_LONG).show();        */
        
        tabHost = getTabHost();
        
        TabSpec tabHomeSpec = tabHost.newTabSpec("Home");
        tabHomeSpec.setIndicator("Home", getResources().getDrawable(R.drawable.icon_photos_tab));
        Intent photosIntent = new Intent(this, HomeTab.class);
        tabHomeSpec.setContent(photosIntent);
        
        TabSpec tabCoursesSpec = tabHost.newTabSpec("Courses");
        tabCoursesSpec.setIndicator("Courses", getResources().getDrawable(R.drawable.icon_videos_tab));
        Intent videosIntent = new Intent(this, CoursesTab.class);
        tabCoursesSpec.setContent(videosIntent);
        
        tabHost.addTab(tabHomeSpec);
        tabHost.addTab(tabCoursesSpec);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
    	getMenuInflater().inflate(R.menu.icon_menu, menu);
    	
    	if(session.isLoggedIn() == true)
    	{
    		menu.add(0, 1, Menu.NONE, name);
			menu.add(0, 4, Menu.NONE, "My Courses");
			menu.add(0, 5, Menu.NONE, "Refresh");
    		menu.add(0, 2, Menu.NONE, "Logout");
    		getMenuInflater().inflate(R.menu.logged_in, menu);
    	}
    	else
    	{
    		menu.add(0, 3, Menu.NONE, "Login");
    	}	
	
		return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	int id = item.getItemId();
    	
    	//if (android.R.id.home == id) {
		//	Intent profile = new Intent(getApplicationContext(), CoursesTab.class);
		//	startActivity(profile);
		//	return true;
		//}
		
		if (id == 1) {
			Intent profile = new Intent(getApplicationContext(),Profile.class);
			startActivity(profile);
			return true;
		}
		
		if(id == 2){
			session.logoutUser();
			return true;
		}
		
		if(id == 3)
		{
			Intent loginAct = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(loginAct);
			return true;
		}
		
		if(id == 4){
			Intent courses = new Intent(getApplicationContext(),MyCourses.class);
			courses.putExtra("uid", uid);
			startActivityForResult(courses, 100);
		}
		
		if(id == 5)
    	{
			finish();
    		startActivity(getIntent());
    		return true;
    	}
		
		if (id == R.id.action_mail)
		{
			Intent msg = new Intent(getApplicationContext(),Messagers.class);
			startActivity(msg);
			return true;
		}
		
		if(id == R.id.ic_action_event)
		{
			//startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			Intent calendar = new Intent(getApplicationContext(), CalendarActivity.class);
			startActivity(calendar);
		}
		
		
	
	return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onRestart() { 
        super.onRestart();
        finish();
		startActivity(getIntent());
    }

}
