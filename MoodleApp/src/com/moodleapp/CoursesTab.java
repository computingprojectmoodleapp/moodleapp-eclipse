package com.moodleapp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


public class CoursesTab extends ListActivity {
	
	// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();
		
		// Session Manager Class
		SessionManager session;
		
		//to get user id and name
		String uid, name;
		
		//to get the menu item to store user's name
		MenuItem itemUser;

	private ProgressDialog pDialog;
	
	JSON_Parse jParser = new JSON_Parse();
	 
    ArrayList<HashMap<String, String>> productsList;
 
    private String url_all_products = session.strUrl + "CourseCategories.php";
 
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "courses";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "name";
 
    JSONArray courses = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.courses_tab_layout);
		
		//object reference to session manager class
  		session = new SessionManager(getApplicationContext());
  		
  		//check login
      	//session.checkLogin();
     
          // get user data from session
          HashMap<String, String> user = session.getUserDetails();
          
          //store user id
          uid = user.get(SessionManager.KEY_USERID);
          
          //store username
          name = user.get(SessionManager.KEY_NAME);
          
         /* //message to indicate user's login status
  	    Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), 
  	        		Toast.LENGTH_LONG).show(); */
		
		productsList = new ArrayList<HashMap<String, String>>();
		 
        // Loading products in Background Thread
        new LoadAllProducts().execute();
 
        // Get listview
        ListView lv = getListView();
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				String item_ID = ((TextView)view.findViewById(R.id.id)).getText().toString();
				String item_Name = ((TextView) view.findViewById(R.id.name)).getText().toString();
				
				Intent in = new Intent(getApplicationContext(), DegreePrograms.class);
				
				in.putExtra(TAG_PID, item_ID);
				in.putExtra(TAG_NAME, item_Name);
				
				startActivityForResult(in, 100);
			}
        	
		});
	}
	
	class LoadAllProducts extends AsyncTask<String, String, String> {
   	 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CoursesTab.this);
            pDialog.setMessage("Loading Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String rootCourse = "0";
            params.add(new BasicNameValuePair("parentID", rootCourse));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);
 
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
 
               if (success == 1) {
                    // products found
                    // Getting Array of Products
                courses = json.getJSONArray(TAG_PRODUCTS);
 
                    // looping through All Products
                    for (int i = 0; i < courses.length(); i++) {
                        JSONObject c = courses.getJSONObject(i);                      
 
                        // Storing each json item in variable
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
 
                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();
 
                        // adding each child node to HashMap key => value
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
 
                        // adding HashList to ArrayList
                        productsList.add(map);
                    }
                } else {
                    // no products found
                    // Launch Add New product Activity
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			
			TextView tv = (TextView) findViewById(R.id.lblCourses);
			tv.setText("COURSE STRUCTURE");
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(CoursesTab.this, productsList, R.layout.list_item, new String[] {TAG_PID, TAG_NAME}, new int[] { R.id.id, R.id.name});
					// updating listview
					setListAdapter(adapter);
				}
			});
		}
	}
	
	@Override
	public void onRestart() { 
	    super.onRestart();
	    //When BACK BUTTON is pressed, the activity on the stack is restarted
	    //Do what you want on the refresh procedure here
	}
}
