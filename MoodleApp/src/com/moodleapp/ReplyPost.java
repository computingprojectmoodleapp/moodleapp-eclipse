package com.moodleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ReplyPost extends Activity{
	
	String forumID;
	String forumtopic;
	String parentID;
	String courseid;
	JSON_Parse jParser = new JSON_Parse();
	JSONArray jArray = null;
	
	EditText txtSubject, txtContent;
	
	AlertDialogManager alert = new AlertDialogManager();
	
	SessionManager session;
	String userid;
	
	String subject;
	String content;
	String msg;
	int result;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reply_post);
		
		Intent intent = getIntent();
		//get discussionID and topic from previous intents
		parentID = intent.getStringExtra("parent");
		forumID = intent.getStringExtra("discussionid");
		forumtopic = intent.getStringExtra("ftopic");
		courseid = intent.getStringExtra("courseid");
		
		Log.d("discussionID: ", forumID);
		Log.d("topic", forumtopic);
		
		//getting userID from session
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		userid = user.get(SessionManager.KEY_USERID);
		
		txtSubject = (EditText) findViewById(R.id.editRSubject);
		txtContent = (EditText) findViewById(R.id.editRMessage);
		
		txtSubject.setText("Re: "+ forumtopic);
		
		Button btnReply = (Button) findViewById(R.id.btnPostReply);
		btnReply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				subject = txtSubject.getText().toString();
				content = txtContent.getText().toString();
				
				if((subject.equals("")) || (content.equals(""))){
					alert.showAlertDialog(ReplyPost.this, "Oops!", "Please fill all fields!", false);
				}
				else{
					//calling background thread to execute 
					new PostReply().execute();
				}
				
			}
		});
		
		Button btnClear = (Button) findViewById(R.id.btnClearRData);
		btnClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//clear the textboxes
				txtSubject.setText("");
				txtContent.setText("");
				
			}
		});
	}
	
	class PostReply extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... args) {
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("discussion", forumID));
			params.add(new BasicNameValuePair("parent", parentID));
			params.add(new BasicNameValuePair("userid", userid));
			params.add(new BasicNameValuePair("subject", subject));
			params.add(new BasicNameValuePair("message", content));
			
			//getting JSON from URL
			JSONObject json = jParser.makeHttpRequest(session.strUrl + "postreply.php", "GET", params);
			
			//check log for JSON result
			Log.d("status: ", json.toString());
			
			try{
				result = json.getInt("success");
				msg = json.getString("message");
	
			}catch(JSONException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String filepath) {
			if(result == 1){
				//alert.showAlertDialog(ReplyPost.this, "Done!", msg, false);
				Toast.makeText(ReplyPost.this, msg, Toast.LENGTH_SHORT).show();
				finish();
				
				Intent forum = new Intent(getApplicationContext(), ForumPosts.class);
				forum.putExtra("id", forumID);
				forum.putExtra("forumtopic", forumtopic);
				forum.putExtra("courseid", courseid);
				startActivityForResult(forum, 100);
			}
			if(result == 0){
				alert.showAlertDialog(ReplyPost.this, "Oops!", msg, false);
			}
		}
	}
	
		
}
